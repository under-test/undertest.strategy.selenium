using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using GlobExpressions;

using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.Git;
using Nuke.Common.Tools.NUnit;
using Nuke.Common.Utilities;
using Nuke.Common.Utilities.Collections;
using UnderTest.Nuke;

using static Nuke.Common.ChangeLog.ChangelogTasks;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.Tools.Git.GitTasks;
using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.NUnit.NUnitTasks;

public class Build : NukeBuild
{
  [SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:ElementsMustAppearInTheCorrectOrder", Justification = "Reviewed. Suppression is OK here.")]
  public static int Main()
  {
    return Execute<Build>(x => x.Pack);
  }

  [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
  public readonly string Configuration = IsLocalBuild ? "Debug" : "Release";

  [Parameter("Api Key for Nuget.org when pushing our nuget package")]
  public readonly string NugetOrgApiKey;

  [Parameter("Slack webhook")]
  public readonly string SlackWebhook;

  [Solution(@"src\UnderTest.Strategy.Selenium.sln")]
  public readonly Solution Solution;

  AbsolutePath SourceDirectory => RootDirectory / "src";

  AbsolutePath ArtifactsDirectory => RootDirectory / "artifacts";

  AbsolutePath TestDirectory => SourceDirectory / "UnderTest.Strategy.Selenium.Tests";

  AbsolutePath AcceptanceTestProjectDirectory => SourceDirectory / "UnderTest.Strategy.Selenium.AcceptanceTests";

  AbsolutePath PackableProject => SourceDirectory / "UnderTest.Strategy.Selenium/UnderTest.Strategy.Selenium.csproj";

  string ChangelogFile => RootDirectory / "CHANGELOG.md";

  IEnumerable<string> ChangelogSectionNotes => ExtractChangelogSectionNotes(ChangelogFile);

  Target Clean =>
    _ =>
      _.Executes(() =>
      {
        GlobDirectories(SourceDirectory, "**/bin", "**/obj")
          .ForEach(DeleteDirectory);

        EnsureCleanDirectory(ArtifactsDirectory);
      });

  Target Restore => _ =>
    _.DependsOn(Clean)
      .Executes(() =>
      {
        DotNetRestore(s => s
          .SetWorkingDirectory(SourceDirectory)
          .SetProjectFile(Solution));
      });

  Target Compile =>
    _ =>
      _.DependsOn(Restore)
        .Executes(() =>
        {
          DotNetBuild(s => s
            .SetWorkingDirectory(SourceDirectory)
            .SetProjectFile(Solution)
            .EnableNoRestore()
            .SetConfiguration(Configuration));
        });

  Target UnitTests =>
    _ =>
      _.DependsOn(Compile)
        .Executes(() =>
        {
          foreach (var project in Solution.GetProjects("*.Tests"))
          {
            DotNetTest(s => s
              .SetConfiguration(Configuration)
              .EnableNoBuild()
              .SetLogger("trx")
              .SetProjectFile(project));
          }
        });

  Target AcceptanceTests =>
    _ =>
      _.DependsOn(UnitTests)
        .Executes(() =>
        {
          Glob
            .Files(AcceptanceTestProjectDirectory, $"bin/**/{Configuration}/**/UnderTest.Strategy.Selenium.AcceptanceTests.dll")
            .ForEach(x =>
            {
              UnderTestTasks.UnderTest(y => y
                .SetProjectFile(Path.Combine(AcceptanceTestProjectDirectory, x)));
            });
        });


  Target Pack => _ => _
    .DependsOn(AcceptanceTests)
    .Executes(() =>
    {
      DotNetPack(s => s
        .SetProject(PackableProject)
        .EnableNoBuild()
        .SetConfiguration(Configuration)
        .EnableIncludeSymbols()
        .SetSymbolPackageFormat(DotNetSymbolPackageFormat.snupkg)
        .SetOutputDirectory(ArtifactsDirectory));
    });

  Target Push =>
    _ =>
      _.DependsOn(Pack)
        .OnlyWhenDynamic(() => OnABranchWeWantToPushToNugetOrg())
        .Requires(() => NugetOrgApiKey)
        .Requires(() => GitHasCleanWorkingCopy())
        .Requires(() => IsReleaseConfiguration())
        .Executes(() =>
        {
          GlobFiles(ArtifactsDirectory, "*.nupkg")
            .ForEach(x =>
            {
              DotNetNuGetPush(s => s
                .SetTargetPath(x)
                .SetSource("https://www.nuget.org/api/v2/package")
                .SetApiKey(NugetOrgApiKey)
                .SetLogOutput(true)
                .SetSkipDuplicate(true));
            });
        });

  bool IsReleaseConfiguration()
  {
    return Configuration.EqualsOrdinalIgnoreCase("Release");
  }

  // our branching strategy is stable for production, release* for releases and hotfix* for hotfix items.
  bool OnABranchWeWantToPushToNugetOrg()
  {
    var branch = this.GetCurrentBranch().ToLowerInvariant();
    Logger.Log(LogLevel.Normal, $"Current branch {branch}");

    return branch == "stable"
           || branch.StartsWith("release")
           || branch.StartsWith("hotfix");
  }

  string GetCurrentBranch()
  {
    var environmentVariable = Environment.GetEnvironmentVariable("APPVEYOR_REPO_BRANCH");
    if (!string.IsNullOrEmpty(environmentVariable))
    {
      return environmentVariable;
    }

    return GitTasks.GitCurrentBranch();
  }
}
