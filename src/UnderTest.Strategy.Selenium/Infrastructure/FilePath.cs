﻿using System;

namespace UnderTest.Strategy.Selenium.Infrastructure
{
  public struct FilePath
  {
    public string Path { get; }

    public FilePath(string path)
    {
      Path = path ?? throw new ArgumentNullException(nameof(path));
    }

    public static FilePath operator / (FilePath left, FilePath right)
    {
      return new FilePath(System.IO.Path.Combine(left.Path, right.Path));
    }

    public static FilePath operator / (FilePath left, string right)
    {
      return new FilePath(System.IO.Path.Combine(left.Path, right));
    }

    public static implicit operator FilePath(string path)
    {
      return new FilePath(path);
    }

    public override string ToString()
    {
      return Path;
    }
  }
}
