using System.Text.RegularExpressions;

namespace UnderTest.Strategy.Selenium.Infrastructure
{
  public class CssLookup
  {
    internal CssLookup(PageElement pageElement)
    {
      this.pageElement = pageElement;
    }

    private readonly PageElement pageElement;

    public string this[string key] => pageElement.GetCssValue(key);

    public string BackgroundImageUrl =>
      Regex
        .Match(pageElement?.GetCssValue("background"),
          @"(url\(.+\))")
            .Value
              .Replace("url(\"", string.Empty)
              .Replace("\")", string.Empty);
  }
}
