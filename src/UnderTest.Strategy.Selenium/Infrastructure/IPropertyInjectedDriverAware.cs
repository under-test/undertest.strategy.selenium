﻿using JetBrains.Annotations;

namespace UnderTest.Strategy.Selenium.Infrastructure
{
  [PublicAPI]
  public interface IPropertyInjectedDriverAware : ICurrentWebDriverAware
  {
    IDriverFactory DriverFactory { get; set; }
    void OnInit();
  }
}
