using JetBrains.Annotations;
using OpenQA.Selenium;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.Infrastructure
{
  [PublicAPI]
  public class PropertyInjectedDriverAware : IPropertyInjectedDriverAware
  {
    [PropertyInjected]
    public IDriverFactory DriverFactory { get; set; }

    [PropertyInjected]
    public IUnderTestSettings TestSettings { get; set; }

    public IWebDriver CurrentDriver => DriverFactory.GetInstance();

    public void OnInit()
    { }
  }
}
