using System;
using System.Runtime.Serialization;
using JetBrains.Annotations;
using UnderTest.Exceptions;

namespace UnderTest.Strategy.Selenium.Exceptions
{
  [PublicAPI]
  [Serializable]
  public abstract class UnderTestSeleniumException : UnderTestException
  {
    protected UnderTestSeleniumException()
    { }

    protected UnderTestSeleniumException(string message)
      : base(message)
    { }

    protected UnderTestSeleniumException(string message, Exception inner)
      : base(message, inner)
    { }

    protected UnderTestSeleniumException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    { }
  }
}
