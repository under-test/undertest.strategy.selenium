using System;
using System.Runtime.Serialization;
using JetBrains.Annotations;
using UnderTest.Exceptions;

namespace UnderTest.Strategy.Selenium.Exceptions
{
  [PublicAPI]
  public class ElementValidationException : UnderTestSeleniumException
  {
    protected ElementValidationException()
    { }

    protected ElementValidationException(string message)
      : base(message)
    { }

    protected ElementValidationException(string message, Exception inner)
      : base(message, inner)
    { }

    protected ElementValidationException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    { }
  }
}
