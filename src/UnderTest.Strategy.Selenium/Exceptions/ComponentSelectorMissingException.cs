﻿using System;
using JetBrains.Annotations;

namespace UnderTest.Strategy.Selenium.Exceptions
{
  [PublicAPI]
  public class ComponentSelectorMissingException : UnderTestSeleniumException
  {
    public ComponentSelectorMissingException(string messages)
      : base(messages)
    { }

    public ComponentSelectorMissingException(string messages, Exception innerException)
      : base(messages, innerException)
    { }
  }
}
