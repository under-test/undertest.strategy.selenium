using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using JetBrains.Annotations;

namespace UnderTest.Strategy.Selenium.Exceptions
{
  [PublicAPI]
  public class ElementShouldHaveClassValidationException : ElementValidationException
  {
    public string ExpectedClass { get; set; }

    public string Selector { get; set; }

    public ElementShouldHaveClassValidationException(string message)
      : base(message)
    { }

    public ElementShouldHaveClassValidationException(string message, Exception inner)
      : base(message, inner)
    { }

    protected ElementShouldHaveClassValidationException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
      ExpectedClass = info.GetString("ExpectedClass");
      Selector = info.GetString("FeatureFileName");
    }

    [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
    public override void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      if (info == null)
      {
        throw new ArgumentNullException(nameof(info));
      }

      info.AddValue("ExpectedClass", ExpectedClass);
      info.AddValue("Selector", Selector);

      base.GetObjectData(info, context);
    }
  }
}
