using System;
using JetBrains.Annotations;
using OpenQA.Selenium;

namespace UnderTest.Strategy.Selenium.Exceptions
{
  [PublicAPI]
  public class WaitTimeoutExceededException : UnderTestSeleniumException
  {
    public WaitTimeoutExceededException(string messages, Exception innerException)
    : base(messages, innerException)
    { }
  }
}
