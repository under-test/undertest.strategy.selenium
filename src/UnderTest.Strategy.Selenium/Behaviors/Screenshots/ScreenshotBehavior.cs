using System.ComponentModel.Composition;
using JetBrains.Annotations;
using OpenQA.Selenium;
using Serilog;
using UnderTest.Strategies;

namespace UnderTest.Strategy.Selenium.Behaviors.Screenshots
{
  [PublicAPI]
  public class ScreenshotBehavior : ITestStrategyEventHandlers, ICurrentWebDriverAware
  {
    [Import]
    public IDriverFactory DriverFactory { get; set; }

    public ScreenshotImageFormat ScreenshotImageFormat { get; set; } = ScreenshotImageFormat.Png;

    public IWebDriver CurrentDriver => DriverFactory.GetInstance();

    public ILogger Log => TestSettings.Logger;

    public IUnderTestSettings TestSettings { get; set; }

    public virtual void BeforeTestRun(BeforeAfterTestRunContext context)
    { }

    public virtual void BeforeFeature(BeforeAfterFeatureContext context)
    { }

    public virtual void BeforeScenario(BeforeAfterScenarioContext context)
    { }

    public virtual void BeforeBackgroundKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    { }

    public virtual void BeforeScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    { }

    public virtual void BeforeStep(BeforeAfterStepContext context)
    { }

    public virtual void AfterFeature(BeforeAfterFeatureContext context)
    { }

    public virtual void AfterScenario(BeforeAfterScenarioContext context)
    { }

    public virtual void AfterScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    { }

    public virtual void AfterBackgroundKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    { }

    public virtual void AfterTestRun(BeforeAfterTestRunContext context)
    { }

    public virtual void AfterStep(BeforeAfterStepContext context)
    {

      TestSettings.Container.GetInstance<IScreenshotService>()
        .Capture(new ScreenshotCaptureContext
        {
          ScreenshotImageFormat = ScreenshotImageFormat,
          TestSettings = TestSettings,
          Feature = context.Feature,
          ScenarioStepResult = context.ScenarioStepResult,
          Scenario = context.Scenario,
          Step = context.Step
        });
    }
  }
}
