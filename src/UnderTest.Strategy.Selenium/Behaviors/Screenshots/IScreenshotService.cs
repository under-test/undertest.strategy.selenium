using OpenQA.Selenium;

namespace UnderTest.Strategy.Selenium.Behaviors.Screenshots
{
  public interface IScreenshotService
  {
    Screenshot Capture(ScreenshotCaptureContext context);
  }
}
