using Gherkin.Ast;
using JetBrains.Annotations;
using OpenQA.Selenium;
using UnderTest.Infrastructure;
using UnderTest.Reporting;

namespace UnderTest.Strategy.Selenium.Behaviors.Screenshots
{
  [PublicAPI]
  public class ScreenshotCaptureContext : IHasTestSettings
  {
    public string Filename { get; set; }

    public ScreenshotImageFormat ScreenshotImageFormat { get; set; }

    public IUnderTestSettings TestSettings { get; set; }

    public FeatureContext Feature { get; set; }

    public ScenarioStepResult ScenarioStepResult { get; set; }

    public Scenario Scenario { get; set; }

    public StepForProcessing Step { get; set; }
  }
}
