using System;
using System.IO;
using JetBrains.Annotations;
using OpenQA.Selenium;
using UnderTest.Reporting;

namespace UnderTest.Strategy.Selenium.Behaviors.Screenshots
{
  [PublicAPI]
  public class ScreenshotService : IScreenshotService, ICurrentWebDriverAware
  {
    public ScreenshotService(IDriverFactory driverFactory, IUnderTestSettings testSettings)
    {
      DriverFactory = driverFactory;
      TestSettings = testSettings;
    }

    public IDriverFactory DriverFactory { get; }

    public IUnderTestSettings TestSettings { get; }

    public IWebDriver CurrentDriver => DriverFactory.GetInstance();

    public Screenshot Capture(ScreenshotCaptureContext context)
    {
      var screenshot = ((ITakesScreenshot)CurrentDriver).GetScreenshot();
      var filename = context.Filename;
      if (string.IsNullOrEmpty(filename))
      {
        var path = Path.Combine(context.TestSettings.OutputSettings.ReportOutputFolder, "screenshots",
          Path.GetFileName(context.Feature.Filename));
        if (!Directory.Exists(path))
        {
          Directory.CreateDirectory(path);
        }

        filename = Path.Combine(path, $"{Path.GetRandomFileName()}.png");
      }

      screenshot.SaveAsFile(filename, context.ScreenshotImageFormat);

      context.ScenarioStepResult.Screenshots.Add(new TestRunScreenshot
      {
        FilePath = filename.Replace(context.TestSettings.OutputSettings.ReportOutputFolder, "."),
        Id = Guid.NewGuid().ToString(),
        Timestamp = DateTime.UtcNow
      });

      return screenshot;
    }
  }
}
