using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using OpenQA.Selenium;

namespace UnderTest.Strategy.Selenium
{
  // todo - we can add other methods/properties from here - https://developer.mozilla.org/en-US/docs/Web/API/ShadowRoot
  [PublicAPI]
  public class ShadowRoot: ICurrentWebDriverAware
  {
    internal ShadowRoot(IWebElement element, ICurrentWebDriverAware parent)
    {
      this.element = element;
      this.parent = parent;
    }


    private IWebElement element;
    private ICurrentWebDriverAware parent;
    public IUnderTestSettings TestSettings => parent.TestSettings;

    public IWebDriver CurrentDriver { get; }

    public IWebElement Element => element;


    public PageElement FindElement(string cssSelector)
    {
      return FindElement(By.CssSelector(cssSelector));
    }

    public PageElement FindPageElement(string cssSelector)
    {
      return FindElement(cssSelector);
    }

    public PageElement FindElement(By by)
    {
      return new PageElement(element.FindElement(by), parent, by);
    }

    public PageElement FindPageElement(By by)
    {
      return FindElement(by);
    }

    public List<PageElement> FindElements(string cssSelector)
    {
      return FindElements(By.CssSelector(cssSelector));
    }

    public List<PageElement> FindElements(By  by)
    {
      return element.FindElements(by)
        .Select(x => new PageElement(x, parent, by))
        .ToList();
    }

    public List<PageElement> FindPageElements(string cssSelector)
    {
      return FindElements(cssSelector);
    }

    public List<PageElement> FindPageElements(By by)
    {
      return FindElements(by);
    }
  }
}
