using JetBrains.Annotations;
using OpenQA.Selenium;
using UnderTest.Attributes;
using UnderTest.Strategy.Selenium.Infrastructure;

namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  public abstract class ComponentBase: PageElement, IPropertyInjectedDriverAware
  {
    public ComponentBase(By selector, ICurrentWebDriverAware page)
      : base(selector, page)
    { }

    protected ComponentBase(IWebElement element, ICurrentWebDriverAware page, By selector)
      : base(element, page, selector)
    { }

    [PropertyInjected]
    public IDriverFactory DriverFactory { get; set; }

    public void OnInit()
    { }
  }
}
