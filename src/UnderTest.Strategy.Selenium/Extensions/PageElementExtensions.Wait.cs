using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using JetBrains.Annotations;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using UnderTest.Strategy.Selenium.Exceptions;

namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  public static class PageElementExtensionsWaits
  {
    public static PageElement WaitUntilActiveElement(this PageElement instance)
    {
      return WaitUntilActiveElement(instance, TimeSpan.Zero);
    }

    public static PageElement WaitUntilActiveElement(this PageElement instance, TimeSpan timeout)
    {
      return instance.WaitUntil(driver =>
      {
        try
        {
          var element = instance.Element;
          if (element == null || !element.Equals(driver.SwitchTo().ActiveElement()))
          {
            return null;
          }

          return instance;
        }
        catch (StaleElementReferenceException)
        {
          return null;
        }
      }, timeout, $"that the active element is {instance.Selector}");
    }

    public static PageElement WaitUntilAvailable(this PageElement instance)
    {
      return WaitUntilAvailable(instance, TimeSpan.Zero);
    }

    public static PageElement WaitUntilAvailable(this PageElement instance, TimeSpan timeout)
    {
      IWebElement element;
      return instance.WaitUntil(driver =>
      {
        try
        {
          element = instance.Element;
          if (element == null)
          {
            return null;
          }
        }
        catch (NoSuchElementException)
        {
          return null;
        }
        catch (WebDriverTimeoutException)
        {
          return null;
        }

        return instance;
      }, timeout, $"find the element {instance.Selector}");
    }

    public static PageElement WaitUntilClickable(this PageElement instance)
    {
      return WaitUntilClickable(instance, TimeSpan.Zero);
    }

    public static PageElement WaitUntilClickable(this PageElement instance, TimeSpan timeout)
    {
      if (timeout == TimeSpan.Zero)
      {
        timeout = DefaultTimeouts.DefaultWaitTimeout;
      }

      return instance.WaitUntil(driver =>
      {
        var element = ElementIfVisible(instance);
        try
        {
          if (element == null || !element.Enabled)
          {
            return null;
          }

          return element;
        }
        catch (StaleElementReferenceException)
        {
          return null;
        }
      }, timeout, $"clickable element {instance.Selector}");
    }

    public static IList<PageElement> WaitUntilCountExists(this PageElement instance, int expectedCount)
    {
      return WaitUntilCountExists(instance, expectedCount, TimeSpan.Zero);
    }

    public static IList<PageElement> WaitUntilCountExists(this PageElement instance, int expectedCount, TimeSpan timeout)
    {
      return instance.WaitUntil(driver =>
      {
        var elements = instance.Parent.FindElements(instance.Selector);
        if (elements.Count != expectedCount)
        {
          return null;
        }

        return elements.ToPageElements(instance);
      }, timeout, $"there to be {expectedCount} {instance.Selector}");
    }

    public static PageElement WaitUntilExists(this PageElement instance)
    {
      return WaitUntilExists(instance, TimeSpan.Zero);
    }

    public static PageElement WaitUntilExists(this PageElement instance, TimeSpan timeout)
    {
      return instance.WaitUntil(driver =>
      {
        var element = instance.Element;
        if (element == null)
        {
          return null;
        }

        return instance;
      }, timeout, $"element {instance.Selector}");
    }

    public static ICurrentWebDriverAware WaitUntilStale(this PageElement instance)
    {
      return WaitUntilStale(instance, TimeSpan.Zero);
    }

    public static ICurrentWebDriverAware WaitUntilStale(this PageElement instance, TimeSpan timeout)
    {
      return instance.WaitUntil(driver =>
      {
        try
        {
          var element = instance.Element;
          return element == null || !element.Enabled ? instance : null;
        }
        catch (StaleElementReferenceException)
        {
          return instance;
        }
      }, timeout, $"the element {instance.Selector} to go stale");
    }

    public static PageElement WaitUntilVisible(this PageElement instance)
    {
      return WaitUntilVisible(instance, TimeSpan.Zero);
    }

    public static PageElement WaitUntilVisible(this PageElement instance, TimeSpan timeout)
    {
      return instance.WaitUntil(driver =>
      {
        try
        {
          return ElementIfVisible(instance);
        }
        catch (StaleElementReferenceException)
        {
          return null;
        }
      }, timeout, $"visible element {instance.Selector}");
    }

    public static PageElement WaitUntilValueIs(this PageElement instance, string value)
    {
      return WaitUntilValueIs(instance, value, TimeSpan.Zero);
    }

    public static PageElement WaitUntilValueIs(this PageElement instance, string value, TimeSpan timeout)
    {
      return instance.WaitUntil(driver =>
      {
        try
        {
          return ElementIfValueIs(instance, value);
        }
        catch (StaleElementReferenceException)
        {
          return (PageElement) null;
        }
      }, timeout, $"element value {instance.Selector} to be `{value}`");
    }

    public static PageElement WaitUntilInvisible(this PageElement instance)
    {
      return WaitUntilInvisible(instance, TimeSpan.Zero);
    }

    public static PageElement WaitUntilInvisible(this PageElement instance, TimeSpan timeout)
    {
      return instance.WaitUntil(driver =>
      {
        try
        {
          return ElementIfInvisible(instance);
        }
        catch (StaleElementReferenceException)
        {
          return (PageElement) null;
        }
      }, timeout, $"invisible element {instance.Selector}");
    }

    private static PageElement ElementIfInvisible(PageElement element)
    {
      return !element.Displayed ? element : null;
    }

    private static PageElement ElementIfVisible(PageElement element)
    {
      return element.Displayed ? element : null;
    }

    public static PageElement ElementIfValueIs(PageElement element, string value)
    {
      return element.GetAttribute("value") == value ? element : null;
    }
  }
}
