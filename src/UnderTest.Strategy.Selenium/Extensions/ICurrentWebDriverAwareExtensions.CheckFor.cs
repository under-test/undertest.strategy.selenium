﻿using System;
using JetBrains.Annotations;
using OpenQA.Selenium;

// ReSharper disable once CheckNamespace
namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  // ReSharper disable once InconsistentNaming
  public static class ICurrentWebDriverAwareExtensionsCheckForExistence
  {
    public static T CheckForExistence<T>(this ICurrentWebDriverAware instance, string selector)
      where T: PageElement
    {
      return instance.CheckForExistence<T>(By.CssSelector(selector));
    }

    public static T CheckForExistence<T>(this ICurrentWebDriverAware instance, By selector)
      where T: PageElement
    {
      try
      {
        PageElement element;
        if (instance is PageElement parent)
        {
          element = parent.FindPageElement(selector) as T;
        }
        else
        {
          element = instance.FindPageElement(selector) as T;
        }

        var result = element?.Displayed;

        return (T) element;
      }
      catch (Exception ex)
      {
        if (ex is not NoSuchElementException)
        {
          throw;
        }

        instance.TestSettings.Logger.Debug($"Failed to locate the {selector}, returning null.");
        return null;

      }
    }
  }
}
