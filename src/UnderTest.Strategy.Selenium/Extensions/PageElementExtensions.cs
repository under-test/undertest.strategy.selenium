using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace UnderTest.Strategy.Selenium
{
  // ReSharper disable once CheckNamespace
  [PublicAPI]
  public static class PageElementExtensions
  {
    // credit for the script to https://gist.github.com/florentbr/349b1ab024ca9f3de56e6bf8af2ac69e
    private const string JsDropFileInjectionScript = "for(var b=arguments[0],k=arguments[1],l=arguments[2],c=b.ownerDocument,m=0;;){var e=b.getBoundingClientRect(),g=e.left+(k||e.width/2),h=e.top+(l||e.height/2),f=c.elementFromPoint(g,h);if(f&&b.contains(f))break;if(1<++m)throw b=Error('Element not interractable'),b.code=15,b;b.scrollIntoView({behavior:'instant',block:'center',inline:'center'})}var a=c.createElement('INPUT');a.setAttribute('type','file');a.setAttribute('style','position:fixed;z-index:2147483647;left:0;top:0;');a.onchange=function(){var b={effectAllowed:'all',dropEffect:'none',types:['Files'],files:this.files,setData:function(){},getData:function(){},clearData:function(){},setDragImage:function(){}};window.DataTransferItemList&&(b.items=Object.setPrototypeOf([Object.setPrototypeOf({kind:'file',type:this.files[0].type,file:this.files[0],getAsFile:function(){return this.file},getAsString:function(b){var a=new FileReader;a.onload=function(a){b(a.target.result)};a.readAsText(this.file)}},DataTransferItem.prototype)],DataTransferItemList.prototype));Object.setPrototypeOf(b,DataTransfer.prototype);['dragenter','dragover','drop'].forEach(function(a){var d=c.createEvent('DragEvent');d.initMouseEvent(a,!0,!0,c.defaultView,0,0,0,g,h,!1,!1,!1,!1,0,null);Object.setPrototypeOf(d,null);d.dataTransfer=b;Object.setPrototypeOf(d,DragEvent.prototype);f.dispatchEvent(d)});a.parentElement.removeChild(a)};c.documentElement.appendChild(a);a.getBoundingClientRect();return a;";

    private static void DropFileOntoElement(this PageElement instance, string filePath, double offsetX = 0, double offsetY = 0)
    {
      if (!File.Exists(filePath))
      {
        throw new FileNotFoundException(filePath);
      }

      var input = instance.ExecuteScript<IWebElement>(JsDropFileInjectionScript, instance, offsetX, offsetY);
      input.SendKeys(filePath);
    }

    public static PageElement EnterValue(this PageElement instance, string valueToEnter)
    {
      instance.Perform(x => x
        .Click()
        .SendKeys(instance.Element, valueToEnter));

      return instance;
    }

    public static PageElement FindPageElementByText(this PageElement instance, string text, string tagToLimitTo = "*")
    {
      return instance.FindPageElement(By.XPath($".//{tagToLimitTo}[text() = '{text}']"));
    }

    public static PageElement FindPageElementByPartialText(this PageElement instance, string text, string tagToLimitTo = "*")
    {
      return instance.FindPageElement(By.XPath($".//{tagToLimitTo}[contains(text(), '" + text + "')]"));
    }

    public static PageElement FindPageElement(this PageElement instance, string cssSelector)
    {
      var selector = By.CssSelector(cssSelector);

      return new PageElement(instance.FindElement(selector), instance, selector);
    }

    public static PageElement FindPageElement(this PageElement instance, By by)
    {
      return new PageElement(instance.FindElement(by), instance, by);
    }

    public static List<PageElement> FindPageElements(this PageElement instance, string cssSelector)
    {
      return instance.FindPageElements(By.CssSelector(cssSelector));
    }

    public static List<PageElement> FindPageElements(this PageElement instance, By by)
    {
      return instance.FindElements(by)
        .Select(x => new PageElement(x, instance,  by))
        .ToList();
    }

    public static List<PageElement> FindPageElementsByText(this PageElement instance, string text, string tagToLimitTo = "*")
    {
      return instance.FindPageElements(By.XPath($".//{tagToLimitTo}[text() = '{text}']"));
    }

    public static List<PageElement> FindPageElementsByPartialText(this PageElement instance, string text, string tagToLimitTo = "*")
    {
      return instance.FindPageElements(By.XPath($".//{tagToLimitTo}[contains(text(), '" + text + "')]"));
    }

    public static PageElement Perform(this PageElement instance, Func<Actions, Actions> whatToPerform)
    {
      whatToPerform(new Actions(instance.CurrentDriver))
        .Build()
        .Perform();

      return instance;
    }

    public static PageElement SetAttribute(this PageElement instance, string attribute, object value)
    {
      instance.ExecuteScript<object>($"arguments[0].setAttribute(arguments[1], arguments[2])", instance, attribute, value);

      return instance;
    }

    public static PageElement ScrollTo(this PageElement instance)
    {
      instance.ExecuteScript<object>("arguments[0].scrollIntoView(true);", instance);

      return instance;
    }
  }
}
