// ReSharper disable once CheckNamespace

using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnderTest.Strategy.Selenium.Exceptions;

// ReSharper disable once CheckNamespace
namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  public static class PageElementToSpecificType
  {
    public static Checkbox ToCheckbox(this PageElement instance)
    {
      return new Checkbox(instance.Selector, instance.Parent);
    }

    public static Checkbox FindCheckbox(this ICurrentWebDriverAware instance, string selector)
    {
      return instance.FindPageElement(selector).ToCheckbox();
    }

    public static IList<Checkbox> FindCheckboxes(this ICurrentWebDriverAware instance, string selector)
    {
      return instance.FindPageElements(selector).Select(x => x.ToCheckbox()).ToList();
    }

    public static Dropdown ToDropdown(this PageElement instance)
    {
      return new Dropdown(instance.Selector, instance.Parent);
    }

    public static Dropdown FindDropdown(this ICurrentWebDriverAware instance, string selector)
    {
      return instance.FindPageElement(selector).ToDropdown();
    }

    public static IList<Dropdown> FindDropdowns(this ICurrentWebDriverAware instance, string selector)
    {
      return instance.FindPageElements(selector).Select(x => x.ToDropdown()).ToList();
    }

    public static Button ToButton(this PageElement instance)
    {
      return new Button(instance.Selector, instance.Parent);
    }

    public static Button FindButton(this ICurrentWebDriverAware instance, string selector)
    {
      return instance.FindPageElement(selector).ToButton();
    }

    public static IList<Button> FindButtons(this ICurrentWebDriverAware instance, string selector)
    {
      return instance.FindPageElements(selector).Select(x => x.ToButton()).ToList();
    }

    public static Textbox ToTextbox(this PageElement instance)
    {
      return new Textbox(instance.Selector, instance.Parent);
    }

    public static Textbox FindTextbox(this ICurrentWebDriverAware instance, string selector)
    {
      return instance.FindPageElement(selector).ToTextbox();
    }

    public static IList<Textbox> FindTextboxes(this ICurrentWebDriverAware instance, string selector)
    {
      return instance.FindPageElements(selector).Select(x => x.ToTextbox()).ToList();
    }

    public static T FindComponent<T>(this PageElement instance)
    {
      return instance.ToComponent<T>();
    }

    public static T FindComponent<T>(this ICurrentWebDriverAware instance)
    {
      if (!(typeof(T).GetCustomAttributes(typeof(SelectorAttribute), true).FirstOrDefault() is SelectorAttribute selectorAttr))
      {
        throw new ComponentSelectorMissingException($"No SelectorAttribute found on the type {typeof(T).Name}.");
      }

      return instance.FindPageElement(selectorAttr.CssSelector).ToComponent<T>();
    }

    public static IList<T> FindComponents<T>(this ICurrentWebDriverAware instance, string selector)
    {
      return instance.FindPageElements(selector).Select(x => x.ToComponent<T>()).ToList();
    }

    public static T ToComponent<T>(this PageElement instance)
    {
      return (T) Activator.CreateInstance(typeof(T), instance.Selector, instance.Parent);
    }

    public static IList<T> ToComponents<T>(this IList<PageElement> instance)
    {
      return  instance
        .Select(x => x.ToComponent<T>())
        .ToList();
    }
  }
}
