using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using JetBrains.Annotations;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using UnderTest.Strategy.Selenium.Exceptions;

// ReSharper disable once CheckNamespace
namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  // ReSharper disable once InconsistentNaming
  public static class ICurrentWebDriverAwareExtensionsWaitUntil
  {
    public static PageElement WaitUntilActiveElement(this ICurrentWebDriverAware instance, string cssSelector)
    {
      return instance.WaitUntilActiveElement(cssSelector, TimeSpan.Zero);
    }

    public static PageElement WaitUntilActiveElement(this ICurrentWebDriverAware instance, string cssSelector, TimeSpan timeout)
    {
      return instance.WaitUntilActiveElement(By.CssSelector(cssSelector), timeout);
    }

    public static PageElement WaitUntilActiveElement(this ICurrentWebDriverAware instance, By by)
    {
      return instance.WaitUntilActiveElement(by, TimeSpan.Zero);
    }

    public static PageElement WaitUntilActiveElement(this ICurrentWebDriverAware instance, By by, TimeSpan timeout)
    {
      return new PageElement(by, instance).WaitUntilActiveElement(timeout);
    }

    public static PageElement WaitUntilAvailable(this ICurrentWebDriverAware instance, string cssSelector)
    {
      return instance.WaitUntilAvailable(By.CssSelector(cssSelector), TimeSpan.Zero);
    }

    public static PageElement WaitUntilAvailable(this ICurrentWebDriverAware instance, string cssSelector, TimeSpan timeout)
    {
      return instance.WaitUntilAvailable(By.CssSelector(cssSelector), timeout);
    }

    public static PageElement WaitUntilAvailable(this ICurrentWebDriverAware instance, By by)
    {
      return instance.WaitUntilAvailable(by, TimeSpan.Zero);
    }

    public static PageElement WaitUntilAvailable(this ICurrentWebDriverAware instance, By by, TimeSpan timeout)
    {
      return new PageElement(by, instance).WaitUntilAvailable(timeout);
    }

    public static PageElement WaitUntilTextExists(this ICurrentWebDriverAware instance, string text)
      => instance.WaitUntilTextExists(text, TimeSpan.Zero);

    public static PageElement WaitUntilTextExists(this ICurrentWebDriverAware instance, string text, TimeSpan timeout)
      => WaitUntil<PageElement>(instance,
        x =>
        {
          var pageElement = instance.FindPageElementByText(text);
          return pageElement.Element != null ? pageElement : null;
        },
        timeout,
        $"finding element with text {text}",
        typeof(NoSuchElementException));

    public static List<PageElement> WaitUntilChildExistsInSelector(this ICurrentWebDriverAware instance,
      string parentSelector,
      Func<PageElement, bool> waitFunc)
    {
      return instance.WaitUntilChildExistsInSelector(parentSelector, waitFunc, TimeSpan.Zero);
    }

    public static List<PageElement> WaitUntilChildExistsInSelector(this ICurrentWebDriverAware instance, string parentSelector,
      Func<PageElement, bool> waitFunc, TimeSpan timeout)
    {
      return instance.WaitUntil(driver =>
      {
        var collection = instance.FindPageElements(parentSelector);

        return !collection.Any(waitFunc) ? null : collection;
      }, timeout, $"waiting for child element that meets criteria in `{parentSelector}`");
    }

    public static PageElement WaitUntilClickable(this ICurrentWebDriverAware instance, string cssSelector)
    {
      return instance.WaitUntilClickable(cssSelector, TimeSpan.Zero);
    }

    public static PageElement WaitUntilClickable(this ICurrentWebDriverAware instance, string cssSelector, TimeSpan timeout)
    {
      return instance.WaitUntilClickable(By.CssSelector(cssSelector), timeout);
    }

    public static PageElement WaitUntilClickable(this ICurrentWebDriverAware instance, By by)
    {
      return instance.WaitUntilClickable(by, TimeSpan.Zero);
    }

    public static PageElement WaitUntilClickable(this ICurrentWebDriverAware instance, By by, TimeSpan timeout)
    {
      return new PageElement(by, instance).WaitUntilClickable(timeout);
    }

    public static IList<PageElement> WaitUntilCountExists(this ICurrentWebDriverAware instance, string cssSelector,
      int expectedCount)
    {
      return instance.WaitUntilCountExists(cssSelector, expectedCount, TimeSpan.Zero);
    }

    public static IList<PageElement> WaitUntilCountExists(this ICurrentWebDriverAware instance, string cssSelector,
      int expectedCount, TimeSpan timeout)
    {
      return instance.WaitUntilCountExists(By.CssSelector(cssSelector), expectedCount, timeout);
    }

    public static IList<PageElement> WaitUntilCountExists(this ICurrentWebDriverAware instance, By by,
      int expectedCount)
    {
      return instance.WaitUntilCountExists(by, expectedCount, TimeSpan.Zero);
    }

    public static IList<PageElement> WaitUntilCountExists(this ICurrentWebDriverAware instance, By by,
      int expectedCount, TimeSpan timeout)
    {
      return instance.WaitUntil(driver =>
      {
        var elements = instance.FindPageElements(by);
        if (elements.Count != expectedCount)
        {
          return null;
        }

        return elements.ToPageElements(instance);
      }, timeout, $"there to be {expectedCount} {by}");
    }

    public static PageElement WaitUntilExists(this ICurrentWebDriverAware instance, string cssSelector)
    {
      return instance.WaitUntilExists(By.CssSelector(cssSelector));
    }

    public static PageElement WaitUntilExists(this ICurrentWebDriverAware instance, string cssSelector, TimeSpan timeout)
    {
      return instance.WaitUntilExists(By.CssSelector(cssSelector), timeout);
    }

    public static PageElement WaitUntilExists(this ICurrentWebDriverAware instance, By by)
    {
      return instance.WaitUntilExists(by, TimeSpan.Zero);
    }

    public static PageElement WaitUntilExists(this ICurrentWebDriverAware instance, By by, TimeSpan timeout)
    {
      return new PageElement(by, instance).WaitUntilExists(timeout);
    }

    public static ICurrentWebDriverAware WaitUntilStale(this ICurrentWebDriverAware instance, IWebElement element, By by, string elementContext = null)
    {
      return instance.WaitUntilStale(element, by, elementContext, TimeSpan.Zero);
    }

    public static ICurrentWebDriverAware WaitUntilStale(this ICurrentWebDriverAware instance, IWebElement element, By by, string elementContext, TimeSpan timeout)
    {
      return new PageElement(element, instance, by).WaitUntilStale(timeout);
    }

    public static ICurrentWebDriverAware WaitUntilTitleIs(this ICurrentWebDriverAware instance, string title)
    {
      return instance.WaitUntilTitleIs(title, TimeSpan.Zero);
    }

    public static ICurrentWebDriverAware WaitUntilTitleIs(this ICurrentWebDriverAware instance, string title, TimeSpan timeout)
    {
      return instance.WaitUntil(
        driver => !driver.Title.Equals(title, StringComparison.InvariantCultureIgnoreCase) ? null : instance,
        timeout,
        $"page title equal to `{title}`");
    }

    public static ICurrentWebDriverAware WaitUntilTitleContains(this ICurrentWebDriverAware instance, string titlePart)
    {
      return instance.WaitUntilTitleContains(titlePart, TimeSpan.Zero);
    }

    public static ICurrentWebDriverAware WaitUntilTitleContains(this ICurrentWebDriverAware instance, string titlePart, TimeSpan timeout)
    {
      return instance.WaitUntil(driver =>
      {
        if (!driver.Title.Contains(titlePart))
        {
          return null;
        }

        return instance;
      }, timeout, $"page title containing `{titlePart}`");
    }

    public static ICurrentWebDriverAware WaitUntilTitleMatch(this ICurrentWebDriverAware instance, string regex)
    {
      return instance.WaitUntilTitleMatch(regex, TimeSpan.Zero);
    }

    public static ICurrentWebDriverAware WaitUntilTitleMatch(this ICurrentWebDriverAware instance, string regex, TimeSpan timeout)
    {
      return instance.WaitUntil(
        driver => !new Regex(regex, RegexOptions.IgnoreCase).Match(driver.Title).Success ? null : instance,
        timeout,
        $"page title containing `{regex}`");
    }

    public static ICurrentWebDriverAware WaitUntilUrlIs(this ICurrentWebDriverAware instance, string url)
    {
      return instance.WaitUntilUrlIs(url, TimeSpan.Zero);
    }

    public static ICurrentWebDriverAware WaitUntilUrlIs(this ICurrentWebDriverAware instance, string url, TimeSpan timeout)
    {
      return instance.WaitUntil(driver =>
        {
          if (!driver.Url.Equals(url, StringComparison.InvariantCultureIgnoreCase))
          {
            return null;
          }

          return instance;
        }, timeout, url);
    }

    public static ICurrentWebDriverAware WaitUntilUrlContains(this ICurrentWebDriverAware instance, string urlPart)
    {
      return instance.WaitUntilUrlContains(urlPart, TimeSpan.Zero);
    }

    public static ICurrentWebDriverAware WaitUntilUrlContains(this ICurrentWebDriverAware instance, string urlPart, TimeSpan timeout)
    {
      return instance.WaitUntil(
        driver => !driver.Url.Contains(urlPart) ? null : instance,
        timeout,
        $"url containing `{urlPart}`");
    }

    public static ICurrentWebDriverAware WaitUntilUrlMatches(this ICurrentWebDriverAware instance, string regex)
    {
      return instance.WaitUntilUrlMatches(regex, TimeSpan.Zero);
    }

    public static ICurrentWebDriverAware WaitUntilUrlMatches(this ICurrentWebDriverAware instance, string regex, TimeSpan timeout)
    {
      return instance.WaitUntil(
        driver => !new Regex(regex, RegexOptions.IgnoreCase).Match(driver.Url).Success ? null : instance,
        timeout,
        regex);
    }

    public static PageElement WaitUntilVisible(this ICurrentWebDriverAware instance, string cssSelector)
    {
      return instance.WaitUntilVisible(cssSelector, TimeSpan.Zero);
    }

    public static PageElement WaitUntilVisible(this ICurrentWebDriverAware instance, string cssSelector, TimeSpan timeout)
    {
      return instance.WaitUntilVisible(By.CssSelector(cssSelector), timeout);
    }

    public static PageElement WaitUntilVisible(this ICurrentWebDriverAware instance, By by)
    {
      return instance.WaitUntilVisible(by, TimeSpan.Zero);
    }

    public static PageElement WaitUntilVisible(this ICurrentWebDriverAware instance, By by, TimeSpan timeout)
    {
      return new PageElement(by, instance).WaitUntilVisible(timeout);
    }

    public static PageElement WaitUntilInvisible(this ICurrentWebDriverAware instance, string cssSelector)
    {
      return instance.WaitUntilInvisible(cssSelector, TimeSpan.Zero);
    }

    public static PageElement WaitUntilInvisible(this ICurrentWebDriverAware instance, string cssSelector, TimeSpan timeout)
    {
      return instance.WaitUntilInvisible(By.CssSelector(cssSelector), timeout);
    }

    public static PageElement WaitUntilInvisible(this ICurrentWebDriverAware instance, By by)
    {
      return instance.WaitUntilInvisible(by, TimeSpan.Zero);
    }

    public static PageElement WaitUntilInvisible(this ICurrentWebDriverAware instance, By by, TimeSpan timeout)
    {
      return new PageElement(by, instance).WaitUntilInvisible(timeout);
    }

    public static PageElement WaitUntilValueIs(this ICurrentWebDriverAware instance, string cssSelector, string value)
    {
      return instance.WaitUntilValueIs(cssSelector, value, TimeSpan.Zero);
    }

    public static PageElement WaitUntilValueIs(this ICurrentWebDriverAware instance, string cssSelector, string value, TimeSpan timeout)
    {
      return instance.WaitUntilValueIs(By.CssSelector(cssSelector), value, timeout);
    }

    public static PageElement WaitUntilValueIs(this ICurrentWebDriverAware instance, By by, string value)
    {
      return instance.WaitUntilValueIs(by, value, TimeSpan.Zero);
    }

    public static PageElement WaitUntilValueIs(this ICurrentWebDriverAware instance, By by, string value, TimeSpan timeout)
    {
      return new PageElement(by, instance).WaitUntilValueIs(value, timeout);
    }

    public static T WaitUntil<T>(this ICurrentWebDriverAware instance, Func<IWebDriver, T> condition, By by)
    {
      return WaitUntil(instance, condition, TimeSpan.Zero, by);
    }

    public static T WaitUntil<T>(this ICurrentWebDriverAware instance, Func<IWebDriver, T> condition, TimeSpan timeout)
    {
      return WaitUntil(instance, condition, timeout, by: null);
    }

    public static T WaitUntil<T>(this ICurrentWebDriverAware instance, Func<IWebDriver, T> condition, TimeSpan timeout, By by)
    {
      return WaitUntil(instance, condition, timeout, by?.ToString(), null);
    }

    public static T WaitUntil<T>(this ICurrentWebDriverAware instance, Func<IWebDriver, T> condition, TimeSpan timeout, string lookupContext, params Type[] exceptionTypes)
    {
      // we can't have a timeout of zero
      if (timeout == TimeSpan.Zero)
      {
        timeout = DefaultTimeouts.DefaultWaitTimeout;
      }

      var waiter = new WebDriverWait(instance.CurrentDriver, timeout)
      {
        PollingInterval = DefaultTimeouts.WaitPollInterval
      };

      if (exceptionTypes != null)
      {
        waiter.IgnoreExceptionTypes(exceptionTypes);
      }

      try
      {
        return waiter.Until(condition);
      }
      catch (WebDriverTimeoutException e)
      {
        var message = DefaultMessageIfWeHaveNoContext<T>(timeout, lookupContext);

        throw new WaitTimeoutExceededException(message, e);
      }
    }

    private static string DefaultMessageIfWeHaveNoContext<T>(TimeSpan timeout, string lookupContext)
    {
      var message = $"Expected condition failed to occur within the timeout of {timeout.TotalSeconds} second(s)";
      if (!string.IsNullOrWhiteSpace(lookupContext))
      {
        message = $"Failed to locate {lookupContext} within the timeout of {timeout.TotalSeconds} second(s)";
      }

      return message;
    }
  }
}
