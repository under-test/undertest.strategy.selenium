using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using UnderTest.Strategy.Selenium.Exceptions;

// ReSharper disable once CheckNamespace
namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  // ReSharper disable once InconsistentNaming
  public static class ICurrentWebDriverAwareExtensions
  {
    public static PageElement FindPageElement(this ICurrentWebDriverAware instance, string cssSelector)
    {
      return new PageElement(By.CssSelector(cssSelector), instance);
    }

    public static PageElement FindPageElement(this ICurrentWebDriverAware instance, By by)
    {
      return new PageElement(by, instance);
    }

    public static PageElement FindPageElementByText(this ICurrentWebDriverAware instance, string text, string tagToLimitTo = "*")
    {
      return instance.FindPageElement(By.XPath($"//{tagToLimitTo}[text() = '{text}']"));
    }

    public static PageElement FindPageElementByPartialText(this ICurrentWebDriverAware instance, string text, string tagToLimitTo = "*")
    {
      return instance.FindPageElement(By.XPath($"//{tagToLimitTo}[contains(text(), '" + text + "')]"));
    }

    public static List<PageElement> FindPageElements(this ICurrentWebDriverAware instance, string cssSelector)
    {
      return FindPageElements(instance, By.CssSelector(cssSelector));
    }

    public static List<PageElement> FindPageElements(this ICurrentWebDriverAware instance, By by)
    {
      return instance.CurrentDriver.FindElements(by)
        .Select(x => new PageElement(x, instance, by))
        .ToList();
    }

    public static List<PageElement> FindPageElementsByText(this ICurrentWebDriverAware instance, string text, string tagToLimitTo = "*")
    {
      return instance.FindPageElements(By.XPath($"//{tagToLimitTo}[text() = '{text}']"));
    }

    public static List<PageElement> FindPageElementsByPartialText(this ICurrentWebDriverAware instance, string text, string tagToLimitTo = "*")
    {
      return instance.FindPageElements(By.XPath($"//{tagToLimitTo}[contains(text(), '" + text + "')]"));
    }

    public static string GetExecutingFolder(this ICurrentWebDriverAware instance)
    {
      return AppDomain.CurrentDomain.BaseDirectory;
    }

    public static T ExecuteScript<T>(this ICurrentWebDriverAware instance, string script, params object[] args)
    {
      return (T) ((IJavaScriptExecutor) instance.CurrentDriver).ExecuteScript(script, args);
    }

    public static decimal GetScrollLocation(this ICurrentWebDriverAware instance)
    {
      var result = instance.ExecuteScript<object>("return window.pageYOffset;");
      return (decimal)Convert.ChangeType(result, TypeCode.Decimal);
    }

    public static ICurrentWebDriverAware ScrollToBottom(this ICurrentWebDriverAware instance)
    {
      instance.ExecuteScript<object>("window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);");

      return instance;
    }

    public static ICurrentWebDriverAware ScrollTo(this ICurrentWebDriverAware instance, int x, int y)
    {
      instance.ExecuteScript<object>("window.scrollTo(arguments[0], arguments[1]);", x, y);

      return instance;
    }

    public static ICurrentWebDriverAware ScrollToTop(this ICurrentWebDriverAware instance)
    {
      instance.ExecuteScript<object>("window.scrollTo(0, 0);");

      return instance;
    }

    public static T CreateComponent<T>(this ICurrentWebDriverAware instance)
      where T: ComponentBase
    {
      if (!(typeof(T).GetCustomAttributes(
        typeof(SelectorAttribute), true
      ).FirstOrDefault() is SelectorAttribute selectorAttr))
      {
        throw new ComponentSelectorMissingException($"No SelectorAttribute found on the type {typeof(T).Name}.");
      }

      var selector = By.CssSelector(selectorAttr.CssSelector);

      return (T) Activator.CreateInstance(typeof(T), selector, instance);
    }
  }
}
