using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnderTest;
using UnderTest.Strategy.Selenium;

// ReSharper disable once CheckNamespace
namespace SimpleInjector
{
  public static class ContainerExtensions
  {
    public static void RegisterHandlersFromAssembly(this Container instance, Assembly assembly, Lifestyle lifestyle = null)
    {
      bool IsFeatureHandlerWeWantRegistered(Type t)
      {
        return t.IsSubclassOf(typeof(FeatureHandler)) && !t.IsAbstract;
      }

      instance.Register<SeleniumFeatureHandler>();

      assembly
        .GetTypes()
        .Where(IsFeatureHandlerWeWantRegistered)
        .ForEach(handler =>
          instance.Register(handler, handler, lifestyle ?? Lifestyle.Scoped));
    }
  }
}
