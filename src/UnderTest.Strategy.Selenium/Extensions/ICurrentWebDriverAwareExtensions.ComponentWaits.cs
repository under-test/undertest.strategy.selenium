﻿// ReSharper disable once CheckNamespace

using OpenQA.Selenium;

namespace UnderTest.Strategy.Selenium
{
  // ReSharper disable once InconsistentNaming
  public static class ICurrentWebDriverAwareElementWaitExtensions
  {
    public static Button WaitForButtonToExist(this ICurrentWebDriverAware instance, string selector)
    {
      return instance.WaitUntilExists(selector).ToButton();
    }

    public static Button WaitForButtonToExist(this ICurrentWebDriverAware instance, By by)
    {
      return instance.WaitUntilExists(by).ToButton();
    }

    public static Checkbox WaitForCheckboxToExist(this ICurrentWebDriverAware instance, string selector)
    {
      return instance.WaitUntilExists(selector).ToCheckbox();
    }

    public static Checkbox WaitForCheckboxToExist(this ICurrentWebDriverAware instance, By by)
    {
      return instance.WaitUntilExists(by).ToCheckbox();
    }

    public static Dropdown WaitForDropdownToExist(this ICurrentWebDriverAware instance, string selector)
    {
      return instance.WaitUntilExists(selector).ToDropdown();
    }

    public static Dropdown WaitForDropdownToExist(this ICurrentWebDriverAware instance, By by)
    {
      return instance.WaitUntilExists(by).ToDropdown();
    }

    public static Textbox WaitForTextboxToExist(this ICurrentWebDriverAware instance, string selector)
    {
      return instance.WaitUntilExists(selector).ToTextbox();
    }

    public static Textbox WaitForTextboxToExist(this ICurrentWebDriverAware instance, By by)
    {
      return instance.WaitUntilExists(by).ToTextbox();
    }
  }
}
