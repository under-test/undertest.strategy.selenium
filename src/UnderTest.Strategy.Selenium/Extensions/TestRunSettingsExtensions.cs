using JetBrains.Annotations;
using SimpleInjector;
using UnderTest.Strategy.Selenium;

// ReSharper disable once CheckNamespace
namespace UnderTest
{
  [PublicAPI]
  public static class TestRunSettingsExtensions
  {
    public static SeleniumTestStrategy AddSeleniumStrategy(this TestRunSettings instance,
      IDriverFactory driverFactory, Configure<SeleniumTestStrategy> configureStrategy)
    {
      var strategy = new SeleniumTestStrategy(instance, driverFactory);
      strategy = SetupInstance(instance, configureStrategy, strategy);

      return strategy;
    }

    public static SeleniumTestStrategy AddSeleniumStrategy(this TestRunSettings instance, Configure<SeleniumTestStrategy> configureStrategy)
    {
      var strategy = new SeleniumTestStrategy(instance);
      strategy = SetupInstance(instance, configureStrategy, strategy);

      return strategy;
    }

    private static SeleniumTestStrategy SetupInstance(TestRunSettings instance, Configure<SeleniumTestStrategy> configureStrategy,
      SeleniumTestStrategy strategy)
    {
      strategy = configureStrategy.InvokeSafe(strategy);

      instance.AddStrategy(strategy);

      instance.Container.Register<IDriverFactory>(() => strategy.DriverFactory, Lifestyle.Singleton);
      return strategy;
    }
  }
}
