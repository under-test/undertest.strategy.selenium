using System.Text;
using UnderTest.Strategy.Selenium;

// ReSharper disable once CheckNamespace
namespace OpenQA.Selenium
{
  // ReSharper disable once InconsistentNaming
  public static class IWebElementExtensions
  {
    public static  By GetXPathFromElement(this IWebElement instance, ICurrentWebDriverAware driver)
    {
      var script = new StringBuilder();

      script.Append("function underTestGetElementXPath(elt)");
      script.Append("{");
      script.Append("  var path = \"\";");
      script.Append("  for (; elt && elt.nodeType == 1; elt = elt.parentNode)");
      script.Append("  {");
      script.Append("    var idx = underTestGetElementIndex(elt);");
      script.Append("    var xname = elt.tagName;");
      script.Append("    if (idx > 1)");
      script.Append("    {");
      script.Append("      xname += \"[\" + idx + \"]\";");
      script.Append("    }");
      script.Append("    path = \"/\" + xname + path;");
      script.Append("  }");
      script.Append("  ");
      script.Append("  return path;");
      script.Append("}");
      script.Append("function underTestGetElementIndex(elt){");
      script.Append("  var index = 1;");
      script.Append("  for (var sibling = elt.previousSibling; sibling; sibling = sibling.previousSibling)");
      script.Append("  {");
      script.Append("    if(sibling.nodeType == 1 && sibling.tagName == elt.tagName)");
      script.Append("    {");
      script.Append("      index++;");
      script.Append("    }");
      script.Append("  }");
      script.Append("  return index;");
      script.Append("}");
      script.Append("return underTestGetElementXPath(arguments[0]).toLowerCase();");

      return By.XPath(driver.ExecuteScript<string>(script.ToString(), instance));
    }
  }
}
