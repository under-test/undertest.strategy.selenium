using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using OpenQA.Selenium;

// ReSharper disable once CheckNamespace
namespace UnderTest.Strategy.Selenium
{
  public static class ReadOnlyCollectionExtensions
  {
    public static ReadOnlyCollection<PageElement> ToPageElements(this IReadOnlyCollection<IWebElement> instance, ICurrentWebDriverAware parent)
    {
      return instance
        .Select(x => new PageElement(x, parent, x.GetXPathFromElement(parent)))
        .ToList()
        .AsReadOnly();
    }
  }
}
