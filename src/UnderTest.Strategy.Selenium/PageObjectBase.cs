using System.Threading.Tasks;
using JetBrains.Annotations;
using UnderTest.Strategy.Selenium.Infrastructure;

namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  public abstract class PageObjectBase : PropertyInjectedDriverAware
  {
    public string PageTitle => CurrentDriver.Title;

    protected abstract string Url { get; }

    protected virtual string WaitForPageLoadSelector { get; set; } = "*";

    public virtual async Task Visit()
    {
      CurrentDriver.Navigate().GoToUrl(Url);
      this.WaitUntilExists(WaitForPageLoadSelector);
    }
  }
}
