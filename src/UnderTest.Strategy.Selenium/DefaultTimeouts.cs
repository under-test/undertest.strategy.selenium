using System;

namespace UnderTest.Strategy.Selenium
{
  public static class DefaultTimeouts
  {
    public static TimeSpan WaitPollInterval { get; internal set; } = TimeSpan.FromMilliseconds(25);

    public static TimeSpan DefaultWaitTimeout { get; internal set; } = TimeSpan.FromSeconds(10);
  }
}
