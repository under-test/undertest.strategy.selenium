﻿using System;

namespace UnderTest.Strategy.Selenium
{
  [AttributeUsage(AttributeTargets.Class)]
  public class SelectorAttribute : Attribute
  {
    public SelectorAttribute(string cssSelector)
    {
      CssSelector = cssSelector;
    }

    public string CssSelector { get; }
  }
}
