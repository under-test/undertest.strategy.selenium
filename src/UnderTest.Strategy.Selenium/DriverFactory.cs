using System;
using JetBrains.Annotations;
using OpenQA.Selenium;
using Serilog;

namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  public class DriverFactory : IDriverFactory
  {
    public DriverFactory(TestRunSettings testRunSettings, DriverLifecycle driverLifecycle, Func<IWebDriver> driverCreation)
    {
      TestSettings = testRunSettings;
      driverLifeCycle = driverLifecycle;
      this.driverCreation = driverCreation;
    }

    private Func<IWebDriver> driverCreation;
    private DriverLifecycle driverLifeCycle { get; set; }

    private IWebDriver driver;

    private bool hasInstanceBeenCalled;

    public bool HasActiveInstance()
    {
      return driver != null;
    }

    public virtual IWebDriver GetInstance()
    {
      hasInstanceBeenCalled = true;
      return driver ??= driverCreation();
    }

    public virtual void SetDriverCreationFunc(Func<IWebDriver> driverCreationFunc)
    {
      if (hasInstanceBeenCalled)
      {
        throw new Exception("SetDriverCreationFunc called after GetInstance has been called.  Configure your driver in your Program.cs");
      }

      driverCreation = driverCreationFunc;
    }

    public virtual void SetDriverLifeCycle(DriverLifecycle driverLifecycle)
    {
      if (hasInstanceBeenCalled)
      {
        throw new Exception("SetDriverLifeCycle called after GetInstance has been called.  Configure your driver in your Program.cs");
      }

      driverLifeCycle = driverLifecycle;
    }

    public ILogger Log { get; }

    public IUnderTestSettings TestSettings { get; }

    public virtual void Dispose()
    {
      ShutdownDriver();
    }

    public virtual void BeforeTestRun(BeforeAfterTestRunContext context)
    { }

    public virtual void BeforeFeature(BeforeAfterFeatureContext context)
    { }

    public virtual void BeforeScenario(BeforeAfterScenarioContext context)
    { }

    public virtual void BeforeBackgroundKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    { }

    public virtual void BeforeScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    { }

    public virtual void BeforeStep(BeforeAfterStepContext context)
    { }

    public virtual void AfterFeature(BeforeAfterFeatureContext context)
    {
      if (driverLifeCycle != DriverLifecycle.Feature)
      {
        return;
      }

      ShutdownDriver();
    }

    public virtual void AfterScenario(BeforeAfterScenarioContext context)
    {
      if (driverLifeCycle != DriverLifecycle.Scenario)
      {
        return;
      }

      ShutdownDriver();
    }

    public virtual void AfterScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    { }

    public virtual void AfterBackgroundKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    { }

    public virtual void AfterStep(BeforeAfterStepContext context)
    { }

    public virtual void AfterTestRun(BeforeAfterTestRunContext context)
    { }

    private void ShutdownDriver()
    {
      driver?.Dispose();
      driver = null;
    }
  }
}
