using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using JetBrains.Annotations;
using OpenQA.Selenium;
using OpenQA.Selenium.Internal;
using UnderTest.Strategy.Selenium.Exceptions;
using UnderTest.Strategy.Selenium.Infrastructure;

namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  public class PageElement : IWebElement, ICurrentWebDriverAware, IWrapsElement
  {
    public PageElement(By selector, ICurrentWebDriverAware page)
    {
      this.selector = selector;
      this.page = page;
    }

    public PageElement(IWebElement element, ICurrentWebDriverAware page, By selector)
    {
      this.element = element;
      this.page = page;
      this.selector = selector;
    }

    private IWebElement element;

    private readonly By selector;

    private readonly ICurrentWebDriverAware page;

    public IUnderTestSettings TestSettings => page.TestSettings;

    [CanBeNull]
    public string Description { get; set; }

    public IWebDriver CurrentDriver => page.CurrentDriver;

    public CssLookup Css => new CssLookup(this);

    public IWebElement Element => element ?? page.CurrentDriver.FindElement(selector);

    public IWebElement WrappedElement => Element;

    public By Selector => selector;

    public ShadowRoot ShadowDom => new ShadowRoot(ExecuteScript<IWebElement>($"return arguments[0].shadowRoot", Element), page);

    public string TagName => Element.TagName;

    public string Text => Element.Text;

    public bool Enabled => Element.Enabled;

    public bool Selected => Element.Selected;

    public Point Location => Element.Location;

    public Size Size  => Element.Size;

    public bool Displayed  => Element.Displayed;

    public bool Exists => CheckIfExists();

    public string Id => GetAttribute("id");

    public string TextContent => Element.GetAttribute("textContent");

    public string InnerHtml => Element.GetAttribute("innerHTML");


    public string NodeText
    {
      get
      {
        var nodeText = ExecuteScript<string>(@"
    var parent = arguments[0];
    var child = parent.firstChild;
    var nodeText = '';
    while (child) {
      if (child.nodeType === Node.TEXT_NODE) {
        nodeText += child.textContent;
      }

      child = child.nextSibling;
    }

    return nodeText;", Element);

        return nodeText;
      }
    }

    public PageElement Parent => new PageElement(ExecuteScript<IWebElement>("return arguments[0].parentNode;", Element), this, Element.GetXPathFromElement(this));


    public PageElement AddClass(string className)
    {
      ExecuteScript<object>("arguments[0].classList.add(arguments[1])", this.Element, className);

      return this;
    }

    private bool CheckIfExists()
    {
      try
      {
        var exists = Element;

        return exists != null;
      }
      catch
      {
        return false;
      }
    }

    public override bool Equals(object obj)
    {
      if (Element == null)
      {
        return obj == null;
      }

      return this.Element.Equals(obj);
    }

    public override int GetHashCode()
    {
      return Element.GetHashCode();
    }

    public IWebElement FindElement(By by)
    {
      return Element.FindElement(by);
    }

    public ReadOnlyCollection<IWebElement> FindElements(By by)
    {
      return Element.FindElements(by);
    }

    public T ExecuteScript<T>(string script, params object[] args)
    {
      return page.ExecuteScript<T>(script, args);
    }

    public void Clear()
    {
      Element.Clear();
    }

    public void SendKeys(string text)
    {
      Element.SendKeys(text);
    }

    public void Submit()
    {
      Element.Submit();
    }

    public void Click()
    {
      this.WaitUntilClickable();
      Element.Click();
    }

    public void Click(int xOffset, int yOffset)
    {
      this.Perform(x => x
        .MoveToElement(Element, xOffset, yOffset)
        .Click());
    }

    public string GetAttribute(string attributeName)
    {
      return Element.GetAttribute(attributeName);
    }

    public string GetProperty(string propertyName)
    {
      return Element.GetProperty(propertyName);
    }

    public string GetCssValue(string propertyName)
    {
      return Element.GetCssValue(propertyName);
    }

    public IList<string> GetClasses()
    {
      return this.GetAttribute("class")?.Split(' ').ToList() ?? new List<string>();
    }

    public PageElement RemoveClass(string className)
    {
      ExecuteScript<object>("arguments[0].classList.remove(arguments[1])", this.Element, className);

      return this;
    }

    public PageElement ShouldBeInvisible()
    {
      if (Displayed)
      {
        throw new Exception($"{selector} should be not visible and is.");
      }

      return this;
    }

    public PageElement ShouldBeVisible()
    {
      if (!Displayed)
      {
        throw new ElementNotVisibleException($"{selector} should be visible and is not.");
      }

      return this;
    }

    public PageElement ShouldExist()
    {
      var exists = Element;

      return this;
    }

    public PageElement ShouldHaveClass(string className)
    {
      if (!GetClasses().Contains(className))
      {
        throw new ElementShouldHaveClassValidationException($"Expected element {selector} to have the class {className}");
      }

      return this;
    }

    public PageElement ShouldNotContainClass(string className)
    {
      if (GetClasses().Contains(className))
      {
        throw new ElementShouldHaveClassValidationException($"Expected element {selector} to have the class {className}");
      }

      return this;
    }
  }
}
