using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using OpenQA.Selenium;

// ReSharper disable once CheckNamespace
namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  public class Dropdown : PageElement
  {
    public Dropdown(By selector, ICurrentWebDriverAware page)
      : base(selector, page)
    { }

    public Dropdown(IWebElement element, ICurrentWebDriverAware page, By selector)
      : base(element, page, selector)
    { }

    public bool Multiple => GetAttribute("multiple")?.ToLowerInvariant() != "false";
    public IList<IWebElement> Options => FindElements(By.TagName("option"));

    public new bool Selected => SelectedOptions.Any();

    [CanBeNull]
    public IWebElement SelectedOption => Options.FirstOrDefault(CheckIfSelected);

    public IList<IWebElement> SelectedOptions => Options.Where(CheckIfSelected).ToList();

    public Dropdown SetSelectedByIndex(int index)
    {
      const string script = @"
arguments[0].selectedIndex = arguments[1]
arguments[0].dispatchEvent(new Event('change', { bubbles: true }));";

      ExecuteScript<dynamic>(script, this, index);

      return this;
    }

    public Dropdown SetSelectedByText(string text)
    {
      const string script = @"for(var i = 0; i < arguments[0].options.length; i++) { if (arguments[0].options[i].text === arguments[1]) { arguments[0].selectedIndex = i; arguments[0].dispatchEvent(new Event('change', { bubbles: true })); return true;  }}";

      var foundAtLeastOne = ExecuteScript<bool>(script, this, text);
      if (!foundAtLeastOne)
      {
        throw new Exception($"No option found matching {text}");
      }

      return this;
    }

    public Dropdown SetSelectedByValue(string value)
    {
      const string script = @"for(var i = 0; i < arguments[0].options.length; i++) { if (arguments[0].options[i].value === arguments[1]) { arguments[0].selectedIndex = i; arguments[0].dispatchEvent(new Event('change', { bubbles: true })); return true;  }}";

      var foundAtLeastOne = ExecuteScript<bool>(script, this, value);
      if (!foundAtLeastOne)
      {
        throw new Exception($"No option found matching {value}");
      }

      return this;
    }

    private static bool CheckIfSelected(IWebElement x)
    {
      return x.GetAttribute("selected") == "true";
    }

    private void SetSelectedIfNotAlreadyValue(IWebElement option, bool expected)
    {
      if (option.Selected == expected)
      {
        return;
      }

      if (expected)
      {
        option.Click();
      }
      else
      {
        ExecuteScript<dynamic>("arguments[0].selected = false; arguments[0].dispatchEvent(new Event('change', { bubbles: true }));", option);
      }
    }
  }
}
