namespace UnderTest.Strategy.Selenium
{
  public enum ButtonType
  {
    Button = 1,
    Reset = 2,
    Submit = 3,
    Unknown = 4
  }
}
