using JetBrains.Annotations;
using OpenQA.Selenium;

// ReSharper disable once CheckNamespace
namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  public class Checkbox : PageElement
  {
    public Checkbox(By selector, ICurrentWebDriverAware page)
      : base(selector, page)
    { }

    public Checkbox(IWebElement element, ICurrentWebDriverAware page, By selector)
      : base(element, page, selector)
    { }

    public bool Checked
    {
      get => this.GetAttribute("checked")?.ToLowerInvariant() == "true";
      set
      {
        if (Checked != value)
        {
          Click();
        }
      }
    }

    public bool Readonly
    {
      get => this.GetAttribute("readonly").ToLowerInvariant() == "readonly";
      set =>
        ExecuteScript<object>(value
          ? "arguments[0].setAttribute('readonly', 'readonly')"
          : "arguments[0].removeAttribute('readonly')", this.Element);
    }

    public string Value
    {
      get => this.GetAttribute("value");
      set => this.SetAttribute("value", value);
    }
  }
}
