using JetBrains.Annotations;
using OpenQA.Selenium;

// ReSharper disable once CheckNamespace
namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  public class Button : PageElement
  {
    public Button(By selector, ICurrentWebDriverAware page)
      : base(selector, page)
    { }

    public Button(IWebElement element, ICurrentWebDriverAware page, By selector)
      : base(element, page, selector)
    { }

    public string FormId => GetAttribute("form");

    public string Name => GetAttribute("name");

    public ButtonType Type =>
      GetAttribute("type")?.ToLowerInvariant() switch
      {
        "button" => ButtonType.Button,
        "reset" => ButtonType.Reset,
        "submit" => ButtonType.Submit,
        _ => ButtonType.Unknown
      };

    public string Value => GetAttribute("value");

    public Button WaitUntilClickableAndClick()
    {
      this.WaitUntilClickable()
        .Click();

      return this;
    }
  }
}
