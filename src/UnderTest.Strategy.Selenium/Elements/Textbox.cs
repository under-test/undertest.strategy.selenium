using JetBrains.Annotations;
using OpenQA.Selenium;

// ReSharper disable once CheckNamespace
namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  public class Textbox : PageElement
  {
    public Textbox(By selector, ICurrentWebDriverAware page)
      : base(selector, page)
    { }

    public Textbox(IWebElement element, ICurrentWebDriverAware page, By selector)
      : base(element, page, selector)
    { }

    public string Value => GetAttribute("value");

    public Textbox SetValue(string value)
    {
      this.EnterValue(value);

      return this;
    }
  }
}
