using JetBrains.Annotations;

namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  public enum DriverLifecycle
  {
    EntireRun = 1,
    Feature = 2,
    Scenario = 3
  }
}
