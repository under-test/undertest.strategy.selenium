using System;
using System.ComponentModel.Composition;
using System.Threading.Tasks;
using JetBrains.Annotations;
using OpenQA.Selenium;

namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  public class SeleniumFeatureHandler<T> :SeleniumFeatureHandler where T: FeatureHandlerServiceBase
  {
    public async Task ExecutesAsync([NotNull]StepsToExecute<T> executor)
    {
      var service = TestSettings.Container.GetInstance<T>();
      await executor.Invoke(service);
    }

    public Task ExecutesAsync([NotNull]Func<Task<T>> whatToExecute)
    {
      return whatToExecute();
    }

    public void ExecutesSync([NotNull]StepsToExecuteSync<T> executor)
    {
      var service = TestSettings.Container.GetInstance<T>();
      executor.Invoke(service);
    }
  }

  [PublicAPI]
  public class SeleniumFeatureHandler : FeatureHandler, ICurrentWebDriverAware
  {
    [Import]
    public IDriverFactory DriverFactory { get; set; }

    public IWebDriver CurrentDriver => DriverFactory.GetInstance();

    public override void AfterFeature(BeforeAfterFeatureContext context)
    {
      DriverFactory.AfterFeature(context);

      base.AfterFeature(context);
    }

    public override void AfterScenario(BeforeAfterScenarioContext context)
    {
      DriverFactory.AfterScenario(context);

      base.AfterScenario(context);
    }
  }
}
