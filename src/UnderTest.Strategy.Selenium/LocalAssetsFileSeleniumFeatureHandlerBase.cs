﻿using System;
using System.IO;
using JetBrains.Annotations;
using UnderTest.Strategy.Selenium.Infrastructure;

namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  public abstract class LocalAssetsFileSeleniumFeatureHandlerBase<T> : SeleniumFeatureHandler<T>
    where T: FeatureHandlerServiceBase
  {
    protected LocalAssetsFileSeleniumFeatureHandlerBase()
    {
      TestFileRoot = new FilePath(this.GetExecutingFolder());
      TestFileRoot = TestFileRoot / "assets" / "pages";
    }

    protected FilePath TestFileRoot { get; }

    protected FilePath TestFilePath { get; set; } = string.Empty;

    public override void BeforeScenario(BeforeAfterScenarioContext context)
    {
      base.BeforeScenario(context);

      TestFilePath = new FilePath(string.Empty);
    }
  }


  [PublicAPI]
  public abstract class LocalAssetsFileSeleniumFeatureHandlerBase :SeleniumFeatureHandler
  {
    protected LocalAssetsFileSeleniumFeatureHandlerBase()
    {
      TestFileRoot = new FilePath(this.GetExecutingFolder());
      TestFileRoot = TestFileRoot / "assets" / "pages";
    }

    protected FilePath TestFileRoot { get; }

    protected FilePath TestFilePath { get; set; } = string.Empty;

    public override void BeforeScenario(BeforeAfterScenarioContext context)
    {
      base.BeforeScenario(context);

      TestFilePath = new FilePath(string.Empty);
    }

    protected virtual void LoadLocalTestFile()
    {
      var filename = Path.Combine(TestFileRoot.Path, TestFilePath.Path);
      if (!File.Exists(filename))
      {
        throw new FileNotFoundException($"Local test file not found {filename}");
      }

      Log.Information($"Loading file: {filename}");
      var uri = new Uri(filename);
      CurrentDriver.Navigate().GoToUrl(uri.AbsoluteUri);
    }
  }
}
