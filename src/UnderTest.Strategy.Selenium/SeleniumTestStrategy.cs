using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;
using OpenQA.Selenium;
using SimpleInjector;
using UnderTest.Strategies;
using UnderTest.Strategy.Selenium.Behaviors.Screenshots;

namespace UnderTest.Strategy.Selenium
{
  [PublicAPI]
  public class SeleniumTestStrategy : ITestStrategy, ISeleniumStrategyBehaviors
  {
    public SeleniumTestStrategy(TestRunSettings settings)
    {
      TestSettings = settings;
      DriverFactory = new DriverFactory(settings, DriverLifecycle.EntireRun, () => throw new Exception("No SetDriverCreationFun called after calling AddSeleniumStrategy."));
    }

    public SeleniumTestStrategy(TestRunSettings settings, IDriverFactory driverFactory)
    {
      TestSettings = settings;
      DriverFactory = driverFactory;
    }

    public TestRunSettings TestSettings { get; }

    public string Name { get; } = "Web browser testing";

    public IDriverFactory DriverFactory { get; }

    public IList<ITestStrategyEventHandlers> StrategyEventHandlers { get; } = new List<ITestStrategyEventHandlers>();


    public ISeleniumStrategyBehaviors AttachBehaviors()
    {
      return this;
    }

    public ISeleniumStrategyBehaviors CaptureScreenshots()
    {
      TestSettings.Container.Register<IScreenshotService, ScreenshotService>();

      var instance = new ScreenshotBehavior
      {
        TestSettings = TestSettings,
        DriverFactory = DriverFactory,
        ScreenshotImageFormat = ScreenshotImageFormat.Png
      };

      StrategyEventHandlers.Add(instance);

      return this;
    }

    public void Dispose()
    {
      DriverFactory?.Dispose();
    }

    public SeleniumTestStrategy EnableDemoMode()
    {
      // causes one second delay between polls on actions
      SetDefaultWaitPollInterval(TimeSpan.FromSeconds(1));

      return this;
    }

    public TestRunSettings FinishAttaching()
    {
      return TestSettings;
    }

    public SeleniumTestStrategy SetDefaultWaitPollInterval(TimeSpan pollInterval)
    {
      DefaultTimeouts.WaitPollInterval = pollInterval;

      return this;
    }

    public SeleniumTestStrategy SetDefaultWaitTimeout(TimeSpan timeout)
    {
      DefaultTimeouts.DefaultWaitTimeout = timeout;

      return this;
    }

    public SeleniumTestStrategy SetDriverCreationFunc(Func<IWebDriver> driverCreationFunc)
    {
      DriverFactory.SetDriverCreationFunc(driverCreationFunc);

      return this;
    }

    public SeleniumTestStrategy SetDriverLifeCycle(DriverLifecycle driverLifecycle)
    {
      DriverFactory.SetDriverLifeCycle(driverLifecycle);

      return this;
    }

    public SeleniumTestStrategy WithFeatureHandlersFrom(Assembly assembly)
    {
      TestSettings.Container.RegisterHandlersFromAssembly(assembly);

      return this;
    }

    public TestRunSettings WithNoBehaviors()
    {
      return TestSettings;
    }

    public SeleniumTestStrategy WithPageObjectsFrom(Assembly assembly)
    {
      assembly
        .GetExportedTypes()
        .Where(type => type.IsClass && !type.IsAbstract && type.IsSubclassOf(typeof(PageObjectBase)))
        .ForEach(x =>
          TestSettings.Container.Register(x, x, Lifestyle.Scoped));

      TestSettings.Container.RegisterInitializer<PageObjectBase>(x => x.OnInit());

      return this;
    }

    public SeleniumTestStrategy WithSitesFrom(Assembly assembly)
    {
      assembly
        .GetExportedTypes()
        .Where(type => type.IsClass && !type.IsAbstract && type.IsSubclassOf(typeof(SiteBase)))
        .ForEach(x =>
          TestSettings.Container.Register(x, x, Lifestyle.Scoped));

      TestSettings.Container.RegisterInitializer<SiteBase>(x => x.OnInit());

      return this;
    }

    public SeleniumTestStrategy WithWebsiteComponentsFrom(Assembly assembly)
    {
      WithSitesFrom(assembly)
        .WithPageObjectsFrom(assembly);

      return this;
    }
  }
}
