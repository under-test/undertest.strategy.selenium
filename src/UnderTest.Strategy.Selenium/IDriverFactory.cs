using System;
using OpenQA.Selenium;
using UnderTest.Strategies;

namespace UnderTest.Strategy.Selenium
{
  public interface IDriverFactory : ITestStrategyEventHandlers, IDisposable
  {
    bool HasActiveInstance();
    IWebDriver GetInstance();
    void SetDriverCreationFunc(Func<IWebDriver> driverCreationFunc);
    void SetDriverLifeCycle(DriverLifecycle driverLifecycle);
  }
}
