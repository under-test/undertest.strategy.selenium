using UnderTest.Infrastructure;

namespace UnderTest.Strategy.Selenium
{
  public interface ISeleniumStrategyBehaviors: IHasInternalTestSettings
  {
    ISeleniumStrategyBehaviors CaptureScreenshots();

    TestRunSettings FinishAttaching();
  }
}
