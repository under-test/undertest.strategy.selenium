using OpenQA.Selenium;
using UnderTest.Infrastructure;

namespace UnderTest.Strategy.Selenium
{
  public interface ICurrentWebDriverAware : IHasTestSettings
  {
    IWebDriver CurrentDriver { get; }
  }
}
