using System;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.StepParams
{
  public class CheckboxCheckedStatusToBoolStepParamAttribute: StepParamAttribute
  {
    public override object Transform(object checkedStatus)
    {
      return checkedStatus switch
      {
        "checked" => true,
        "unchecked" => false,
        _ => throw new Exception($"Unknown selector lookup {checkedStatus}")
      };
    }
  }
}
