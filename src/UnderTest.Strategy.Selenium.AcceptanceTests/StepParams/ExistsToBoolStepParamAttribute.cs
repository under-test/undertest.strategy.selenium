using System;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.StepParams
{
  public class ExistsToBoolStepParamAttribute: StepParamAttribute
  {
    public override object Transform(object checkedStatus)
    {
      return checkedStatus switch
      {
        "exists" => true,
        "does not exist" => false,
        _ => throw new Exception($"Unknown selector lookup {checkedStatus}")
      };
    }
  }
}
