using System;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.StepParams
{
  public class DropdownSelectedStatusToBoolStepParamAttribute: StepParamAttribute
  {
    public override object Transform(object selectedStatus)
    {
      return selectedStatus switch
      {
        "selected" => true,
        "not selected" => false,
        _ => throw new Exception($"Unknown selector lookup {selectedStatus}")
      };
    }
  }
}
