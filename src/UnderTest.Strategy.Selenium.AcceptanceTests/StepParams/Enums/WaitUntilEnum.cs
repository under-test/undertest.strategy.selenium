using UnderTest.Support;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.StepParams.Enums
{
  public enum WaitUntilEnum
  {
    [EnumAlias("visible")]
    Visible,

    [EnumAlias("clickable")]
    Clickable,

    [EnumAlias("invisible")]
    Invisible,

    [EnumAlias("exists")]
    ElementExists,

    [EnumAlias("title-equals")]
    TitleEquals,

    [EnumAlias("title-contains")]
    TitleContains,

    [EnumAlias("title-match")]
    TitleMatch,

    [EnumAlias("url-contains")]
    UrlContains,

    [EnumAlias("wait-for-none")]
    WaitForNoneToExist,

    [EnumAlias("wait-for-all")]
    WaitForAllWeExpectToExist,

    [EnumAlias("stale")]
    ElementBecomesStale,

    [EnumAlias("active-element")]
    ActiveElement
  }
}
