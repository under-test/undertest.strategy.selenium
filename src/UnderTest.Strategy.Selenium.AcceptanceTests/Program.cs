using System;
using System.Diagnostics.CodeAnalysis;
using OpenQA.Selenium.Chrome;

namespace UnderTest.Strategy.Selenium.AcceptanceTests
{
  [ExcludeFromCodeCoverage]
  class Program
  {
    static int Main(string[] args)
    {
      var assembly = typeof(Program).Assembly;
      return new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("UnderTest.Strategy.Selenium")
          .SetProjectVersionFromAssembly(typeof(SeleniumTestStrategy).Assembly))
        .WithTestSettings(settings => settings
          .AddAssembly(assembly)
          .AddSeleniumStrategy(strategy => strategy
            .WithFeatureHandlersFrom(assembly)
            .SetDriverCreationFunc(() => new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory))
            .SetDriverLifeCycle(DriverLifecycle.EntireRun)
            .WithWebsiteComponentsFrom(assembly))
            .AttachBehaviors()
              .CaptureScreenshots()
              .FinishAttaching())
        .Execute()
          .ToExitCode();
    }
  }
}
