Feature: Wait until should fail with useful information

  Scenario Outline: Failure should throw with a clear message
    Given our test page
    When we wait for the element to <what-to-wait-for>
    Then an exception should be thrown with a clear message
    Examples: types of waits
      | what-to-wait-for |
      | visible          |
      | clickable        |
      | exists           |
      | title-equals     |
      | title-contains   |
      | title-match      |
      | url-contains     |
      | invisible        |
      | wait-for-all     |
      | stale            |
      | active-element   |

  Scenario: Failed to find component
    Given our test page
    When we wait for an element that does not exist
    Then an exception should be thrown

  Scenario: No lookup context
    Given our test page
    When we wait for the element with no context
    Then an exception, without context, should be thrown
