﻿Feature: Collect a Component from a page

  Scenario: Simple capture of the Component
    Given our test page
    When we capture the Component
    Then the Component should have the properties of the PageElement
      And the child properties should be available
