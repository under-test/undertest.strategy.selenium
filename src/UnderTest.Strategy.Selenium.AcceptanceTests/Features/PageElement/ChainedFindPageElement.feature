Feature: Find nested PageElements

  Scenario: Find within a parent element
    Given our test page
    When we find the child element with the content 'in a child'
    Then we should find the child element

  Scenario: Chained nested elements
    Given our test page
    When we locate the second row
    Then we should find the second row

  Scenario: Chained nested elements with a wait
    Given our test page
    When we locate the second row after waiting
    Then we should find the second row
