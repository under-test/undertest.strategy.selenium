Feature: Scroll to a PageElement on a page

  Scenario: Simple scroll to a PageElement
    Given our test page
    When we scroll to the PageElement
    Then the element should be visible
