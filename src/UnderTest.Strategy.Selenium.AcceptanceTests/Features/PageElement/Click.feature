﻿Feature: Click elements for sport and fun

  As a developer
  I want to click elements
  So I can interact with them

  Scenario Outline: clicking
    Given our test page
    When I click call Click() on <context>
    Then the element should have been clicked
    Examples:
      | context                    |
      | basic                      |
      | disabled-for-three-seconds |
