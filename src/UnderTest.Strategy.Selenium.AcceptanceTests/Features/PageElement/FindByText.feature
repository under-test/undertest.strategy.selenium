Feature: Find PageElements by their text contents

  Scenario: Exact match
    Given our test page
    When we find the element with the content 'shoes'
    Then we should find the element

  Scenario: Find within a parent element
    Given our test page
    When we find the child element with the content 'in a child'
    Then we should find the child element

  Scenario: Find by partial text
    Given our test page
    When we find the element by the partial content 'something'
    Then we should find the partial element
