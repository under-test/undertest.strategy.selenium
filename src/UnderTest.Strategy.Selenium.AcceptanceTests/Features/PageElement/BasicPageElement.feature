Feature: Collect a PageElement from a page

  Scenario: Simple capture of the PageElement
    Given our test page
    When we capture the PageElement
    Then the PageElement should have the properties of the IWebElement

  Scenario: Simple capture of the PageElement by Text
    Given our test page
    When we capture the PageElement by Text
    Then the PageElement should have the properties of the IWebElement

  Scenario: Locate parent element
    Given our test page
    When we capture the PageElement's parent
    Then the PageElement should be the parent element

  Scenario: Get CSS properties
    Given our test page
    When we capture out test PageElement's background-color
    Then it should be blue

  Scenario: Get an attribute
    Given our test page
    When we capture the PageElement
    Then the title should be "also shoes"

  Scenario: Get an attribute
    Given our test page
    When we capture out test TextNode element
    Then the NodeText should be "also shoes"

  Scenario Outline: Check if an element exists
    Given our test page
    When we check if an element that <does-exist>
    Then when we check if it exists, the answer should be <exist-result>

    Examples: Exists inputs
    | does-exist      | exist-result  |
    | exists          | true          |
    | does not exist  | false         |
