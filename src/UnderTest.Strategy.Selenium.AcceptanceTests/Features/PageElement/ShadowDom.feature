Feature: Access to ShadowDom

  Scenario: Simple access to the shadow DOM via PageElement
    Given our test page
    When we capture the PageElement
    Then the shadow dom element is available
    And the shadow dom's children are accessible
