Feature: Verify page element status

  Scenario: Simple visibility verification of a visible PageElement
    Given our browser is on a page where our test element exists onscreen
    Then the element should be visible

  Scenario: Simple visibility verification of a invisible PageElement
    Given our browser is on a page where our test element does not exist onscreen
    Then the element should be invisible

  Scenario Outline: Simple existence verification of a  PageElement
    Given our browser is on a page where our test element <exists> onscreen
    Then the element <exists>
    Examples:
    | exists         |
    | exists         |
    | does not exist |
