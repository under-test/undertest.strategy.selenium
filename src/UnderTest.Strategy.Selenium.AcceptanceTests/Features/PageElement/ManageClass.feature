Feature: Manage Class

  As a developer
  I want to be able to manage the class on elements
  So that I can control elements through their classes

  Scenario: Get the classes on an element
    Given our test page
    When we capture the PageElement
    Then the PageElement should have the expected classes

  Scenario: Add a class to an element
    Given our test page
    When we capture the PageElement
      And we add a new class
    Then the PageElement should have the expected classes
      And the class we added

  Scenario: Remove a class from an element
    Given our test page
    When we capture the PageElement
      And we remove a new class
    Then the PageElement should only have the class we didn't remove
