Feature: Basic dropdown interactions

  Scenario Outline: Selected status
    Given our test page
    When we capture the <selected-status> Dropdown
    Then the element should be <selected-status>

    Examples: selected statuses
      | selected-status  |
      | selected         |
      | not selected     |

  Scenario Outline: Set selected status
    Given our test page
    When we capture the <org-selected-status> Dropdown
    And we mark the dropdown as selected
    Then the element should be selected

    Examples: check statuses
      | org-selected-status |
      | selected            |
      | not selected        |
