Feature: Basic checkbox interactions

  Scenario Outline: Check checked status
    Given our test page
    When we capture the <checked-status> Checkbox
    Then the element should be <checked-status>

    Examples: check statuses
    | checked-status   |
    | checked          |
    | unchecked        |

  Scenario Outline: Set checked status
    Given our test page
    When we capture the <org-checked-status> Checkbox
      And we mark the checkbox as <new-checked-status>
    Then the element should be <new-checked-status>

    Examples: check statuses
      | org-checked-status | new-checked-status |
      | checked            | unchecked          |
      | unchecked          | checked            |
