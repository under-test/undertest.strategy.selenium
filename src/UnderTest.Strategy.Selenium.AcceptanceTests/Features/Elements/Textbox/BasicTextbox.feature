Feature: Basic textbox interactions

  Scenario: Value
    Given our test page
    When we enter "shoes" into the textbox
    Then the value should be "shoes"
