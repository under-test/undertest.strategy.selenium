Feature: Basic button interactions

  Scenario: Click
    Given our test page
    When we click the button
    Then the element should have been clicked
