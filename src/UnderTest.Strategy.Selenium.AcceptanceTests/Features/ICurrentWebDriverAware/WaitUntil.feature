Feature: Waiting for constraints

  Scenario Outline: Waits on elements
    Given our test page
    When we wait for the element to be <what-to-wait-for>
    Then the element is <what-to-wait-for>
    Examples: types of waits
    | what-to-wait-for |
    | visible          |
    | clickable        |
    | exists           |
    | title-equals     |
    | title-contains   |
    | title-match      |
    | url-contains     |
    | invisible        |
    | wait-for-none    |
    | wait-for-all     |
    | stale            |
    | active-element   |
    | child-element    |
    | text-exists      |
