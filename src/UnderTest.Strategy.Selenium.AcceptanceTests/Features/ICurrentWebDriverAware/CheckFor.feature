﻿Feature: Check for existence

  Scenario Outline: Checks for element existence
    Given our test page
    When we check for the element to be <what-to-check-for>
    Then the element is <what-to-check-for>
    Examples: types of checks
      | what-to-check-for   |
      | simple-check        |
      | non-existent        |
      | nested-that-exists  |
      | nested-doesnt-exist |
