Feature: Scroll to

  As a developer
  I want to be able to scroll to specific points on the page
  So that I can control the scroll position

  Scenario Outline: Scroll to
    Given our browser test page
    When we scroll to <location>
    Then the scroll location should be <location>
  Examples:
    | location              |
    | the top               |
    | the bottom            |
    | a specific-location   |
