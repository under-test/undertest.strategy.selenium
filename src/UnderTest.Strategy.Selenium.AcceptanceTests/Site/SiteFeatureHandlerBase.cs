using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.Site
{
  public abstract class SiteFeatureHandlerBase : SeleniumFeatureHandler
  {
    [PropertyInjected]
    public Site Site { get; set; }
  }
}
