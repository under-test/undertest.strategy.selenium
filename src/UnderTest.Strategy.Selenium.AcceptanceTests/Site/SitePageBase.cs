using System;
using System.IO;
using System.Threading.Tasks;
using Serilog;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.Site
{
  public abstract class SitePageBase: PageObjectBase
  {
    [PropertyInjected]
    public SiteConfig Config { get; set; }

    protected abstract override string Url { get; }

    public override Task Visit()
    {
      var filename = Path.Combine(this.GetExecutingFolder(), Config.PagesPath, Url);
      if (!File.Exists(filename))
      {
        throw new FileNotFoundException($"Local test file not found {filename}");
      }

      Log.Information($"Loading file: {filename}");
      var uri = new Uri(filename);
      CurrentDriver.Navigate().GoToUrl(uri.AbsoluteUri);

      return base.Visit();
    }
  }
}
