using FluentAssertions;
using JetBrains.Annotations;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.Site.Pages
{
  [UsedImplicitly]
  public class CssBasicPropertiesPage : SitePageBase
  {
     protected override string Url { get; } = @"css/basic.html";

     private PageElement ElementWithBackground => this.FindPageElement("#element-with-background");

    public void VerifyBackgroundExists() =>
      ElementWithBackground.ShouldExist();

    public void VerifyBackgroundIsOurExampleImage() =>
      ElementWithBackground
        .Css
        .BackgroundImageUrl
        .Should()
        .EndWith("img/monday.jpg");
  }
}
