using System;
using FluentAssertions;
using JetBrains.Annotations;
using OpenQA.Selenium;
using Serilog;
using UnderTest.Strategy.Selenium.AcceptanceTests.Site.Components;
using UnderTest.Strategy.Selenium.AcceptanceTests.StepParams.Enums;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.Site.Pages
{
  [UsedImplicitly]
  public class WaitUntilPage : SitePageBase
  {
    private PageElement element;
    private ExampleComponent Component => new ExampleComponent(By.CssSelector(".component"), this);
    protected override string Url { get; } = @"wait\WaitUntil.html";

    public void WaitForElement(WaitUntilEnum whatToWaitFor)
    {
      switch (whatToWaitFor)
      {
        case WaitUntilEnum.Visible:
          element = Component.WaitUntilVisible("#hidden-for-three-seconds");
          break;
        case WaitUntilEnum.Clickable:
          element = Component.WaitUntilClickable("#disabled-for-three-seconds");
          break;
        case WaitUntilEnum.Invisible:
          element = Component.WaitUntilInvisible("#will-hide-after-three-seconds");
          break;
        case WaitUntilEnum.ElementExists:
          element = Component.WaitUntilExists("#exists-after-three-seconds");
          break;
        case WaitUntilEnum.TitleEquals:
          Component.WaitUntilTitleIs("updated");
          break;
        case WaitUntilEnum.TitleContains:
          Component.WaitUntilTitleContains("something");
          break;
        case WaitUntilEnum.TitleMatch:
          Component.WaitUntilTitleMatch(@"(.*) something");
          break;
        case WaitUntilEnum.UrlContains:
          Component.WaitUntilUrlContains(@"WaitUntil.html");
          break;
        case WaitUntilEnum.WaitForNoneToExist:
          Component.WaitUntilCountExists(".does-not-exist", 0);
          break;
        case WaitUntilEnum.WaitForAllWeExpectToExist:
          Component.WaitUntilCountExists(".wait-for-all", 5);
          break;
        case WaitUntilEnum.ElementBecomesStale:
          Component.WaitUntilStale(this.FindPageElement("#will-be-stale").Element, By.CssSelector("#will-be-stale"));
          break;
        case WaitUntilEnum.ActiveElement:
          element = Component.WaitUntilActiveElement("#focus-after-three-seconds");
          break;
        default:
          throw new Exception($"Unknown <whatToWaitFor> {whatToWaitFor}");
      }
    }

    public void VerifyElementExistence(WaitUntilEnum whatToWaitFor)
    {
      switch (whatToWaitFor)
      {
        case WaitUntilEnum.Visible:
          element.Displayed.Should().BeTrue();
          break;
        case WaitUntilEnum.Clickable:
          (element.Displayed && element.Enabled).Should().BeTrue();
          break;
        case WaitUntilEnum.Invisible:
          element.Displayed.Should().BeFalse();
          break;
        case WaitUntilEnum.ElementExists:
          element.Should().NotBeNull();
          break;
        case WaitUntilEnum.TitleEquals:
          CurrentDriver.Title.Should().Be("updated");
          break;
        case WaitUntilEnum.TitleContains:
          CurrentDriver.Title.Should().Contain("something");
          break;
        case WaitUntilEnum.TitleMatch:
          CurrentDriver.Title.Should().Contain("something");
          break;
        case WaitUntilEnum.UrlContains:
          CurrentDriver.Url.Should().Contain("WaitUntil.html");
          break;
        case WaitUntilEnum.WaitForNoneToExist:
          Component.FindPageElements(".wait-for-none").Count.Should().Be(0);
          break;
        case WaitUntilEnum.WaitForAllWeExpectToExist:
          Component.FindPageElements(".wait-for-all").Count.Should().Be(5);
          break;
        case WaitUntilEnum.ElementBecomesStale:
          Component.FindPageElements("#will-be-stale").Count.Should().Be(0);
          break;
        case WaitUntilEnum.ActiveElement:
          element.Equals(CurrentDriver.SwitchTo().ActiveElement()).Should().BeTrue();
          break;
        default:
          throw new Exception($"Unknown <whatToWaitFor> {whatToWaitFor}");
      }

      Log.Information($"  Waited for element to be {whatToWaitFor}");
    }

    public Action BuildElementLookup(WaitUntilEnum whatToWaitFor)
    {
      var timeout = TimeSpan.FromMilliseconds(500);
      var component = this.FindPageElement(".component")
        .ToComponent<ExampleComponent>();

      return whatToWaitFor switch
      {
        WaitUntilEnum.Visible => (Action) (() => component.WaitUntilVisible("#just-here-and-invisible", timeout)),
        WaitUntilEnum.Clickable => (() => component.WaitUntilClickable("#just-here-and-invisible", timeout)),
        WaitUntilEnum.Invisible => (() => component.WaitUntilInvisible("#just-here-and-visible", timeout)),
        WaitUntilEnum.ElementExists => (() => component.WaitUntilExists("#never-exists", timeout)),
        WaitUntilEnum.TitleEquals => (() => component.WaitUntilTitleIs("something it will never be", timeout)),
        WaitUntilEnum.TitleContains => (() => component.WaitUntilTitleContains("nothing", timeout)),
        WaitUntilEnum.TitleMatch => (() => component.WaitUntilTitleMatch(@"(.*) nothing", timeout)),
        WaitUntilEnum.UrlContains => (() => component.WaitUntilUrlContains(@"Nope.html", timeout)),
        WaitUntilEnum.WaitForAllWeExpectToExist => (() =>
          component.WaitUntilCountExists(".just-here-and-visible", 5, timeout)),
        WaitUntilEnum.ElementBecomesStale => (() =>
          component.WaitUntilStale(this.FindPageElement("#just-here-and-visible").Element,
            By.CssSelector("#just-here-and-visible"), "#just-here-and-visible", timeout)),
        WaitUntilEnum.ActiveElement => (() => component.WaitUntilActiveElement("#just-here-and-visible", timeout)),
        _ => throw new Exception($"Unknown <whatToWaitFor> {whatToWaitFor}")
      };
    }
  }
}
