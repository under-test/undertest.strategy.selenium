using UnderTest.Configuration;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.Site
{
  public class SiteConfig : UnderTestCommandLineOptions
  {
    public string PagesPath => @"assets/pages";
  }
}
