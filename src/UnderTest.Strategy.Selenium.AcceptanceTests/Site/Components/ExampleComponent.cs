using OpenQA.Selenium;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.Site.Components
{
  public class ExampleComponent : ComponentBase
  {
    public ExampleComponent(By selector, ICurrentWebDriverAware page)
      : base(selector, page)
    { }
  }
}
