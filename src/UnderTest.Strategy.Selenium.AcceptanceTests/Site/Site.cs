using UnderTest.Attributes;
using UnderTest.Strategy.Selenium.AcceptanceTests.Site.Pages;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.Site
{
  public class Site : SiteBase
  {
    [PropertyInjected]
    public SiteConfig Config { get; set; }

    [PropertyInjected]
    public CssBasicPropertiesPage CssBasicProperties { get; set; }

    [PropertyInjected]
    public WaitUntilPage WaitUntil { get; set; }
  }
}
