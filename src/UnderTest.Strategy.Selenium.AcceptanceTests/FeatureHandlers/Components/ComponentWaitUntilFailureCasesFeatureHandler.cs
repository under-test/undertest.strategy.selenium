using System;
using FluentAssertions;
using OpenQA.Selenium;
using UnderTest.Attributes;
using UnderTest.Strategy.Selenium.AcceptanceTests.Site;
using UnderTest.Strategy.Selenium.AcceptanceTests.Site.Components;
using UnderTest.Strategy.Selenium.AcceptanceTests.StepParams.Enums;
using UnderTest.Strategy.Selenium.Exceptions;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"Components/WaitUntilFailureCases.feature")]
  public class ComponentWaitUntilFailureCasesFeatureHandler : SiteFeatureHandlerBase
  {
    private Action elementLookup;
    private WaitUntilEnum whatToWaitFor;

    [Given("our test page")]
    public void GivenWeAreOnOurTestPage() =>
      Site.WaitUntil.Visit();

    [When("we wait for the element to <what-to-wait-for>")]
    public void WhenWeWaitForTheElementToExist(WaitUntilEnum whatToWaitFor)
    {
      elementLookup = Site.WaitUntil.BuildElementLookup(whatToWaitFor);
      this.whatToWaitFor = whatToWaitFor;
    }

    [When("we wait for the element with no context")]
    public void WeWaitForTheElementWithNoContext()
    {
      elementLookup = () =>
      {
        var initialWindowCount = CurrentDriver.WindowHandles.Count;
        new Selenium.PageElement(By.Id("example-link"), this).WaitUntilClickable().Click();
        // + 2 what causes this to fail.
        this.WaitUntil(driver => driver.WindowHandles.Count > initialWindowCount + 2, TimeSpan.FromSeconds(1),
          // this is what we are testing
          lookupContext: null);
      };
    }

    [When("we wait for an element that does not exist")]
    public void WhenWeWAitForAndElementThatDoesNotExist()
    {
      elementLookup = () => this.FindPageElement("I Do Not Exist").ToComponent<ExampleComponent>();
    }

    [Then("an exception should be thrown")]
    public void ThenAnExceptionShouldBeThrown()
    {
      const string message = "no such element: Unable to locate element: {\"method\":\"css selector\",\"selector\":\"I Do Not Exist\"}";

      elementLookup
        .Should().Throw<NoSuchElementException>()
        .And
        .Message.Should().Contain(message);
    }

    [Then("an exception should be thrown with a clear message")]
    public void ThenAnExceptionShouldBeThrownWithAClearMessage()
    {
      var message = BuildMessageFromWhatToWaitFor();

      elementLookup
        .Should().Throw<WaitTimeoutExceededException>()
        .And
        .Message.Should().Be(message);
    }

    [Then("an exception, without context, should be thrown")]
    public void AnExceptionWithoutContextShouldBeThrown()
    {
      elementLookup
        .Should().Throw<WaitTimeoutExceededException>()
        .And
        .Message.Should().Be("Expected condition failed to occur within the timeout of 1 second(s)");
    }

    private string BuildMessageFromWhatToWaitFor()
    {
      string BuildMessage(string context) => $"Failed to locate {context} within the timeout of 0.5 second(s)";

      return whatToWaitFor switch
      {
        WaitUntilEnum.Visible => BuildMessage("visible element By.CssSelector: #just-here-and-invisible"),
        WaitUntilEnum.Clickable => BuildMessage("clickable element By.CssSelector: #just-here-and-invisible"),
        WaitUntilEnum.Invisible => BuildMessage("invisible element By.CssSelector: #just-here-and-visible"),
        WaitUntilEnum.ElementExists => BuildMessage("element By.CssSelector: #never-exists"),
        WaitUntilEnum.TitleEquals => BuildMessage("page title equal to `something it will never be`"),
        WaitUntilEnum.TitleContains => BuildMessage("page title containing `nothing`"),
        WaitUntilEnum.TitleMatch => BuildMessage(@"page title containing `(.*) nothing`"),
        WaitUntilEnum.UrlContains => BuildMessage(@"url containing `Nope.html`"),
        WaitUntilEnum.WaitForAllWeExpectToExist => BuildMessage("there to be 5 By.CssSelector: .just-here-and-visible"),
        WaitUntilEnum.ElementBecomesStale => BuildMessage("the element By.CssSelector: #just-here-and-visible to go stale"),
        WaitUntilEnum.ActiveElement => BuildMessage("that the active element is By.CssSelector: #just-here-and-visible"),
        _ => throw new Exception(BuildMessage($"Unknown <whatToWaitFor> {whatToWaitFor}"))
      };
    }
  }
}
