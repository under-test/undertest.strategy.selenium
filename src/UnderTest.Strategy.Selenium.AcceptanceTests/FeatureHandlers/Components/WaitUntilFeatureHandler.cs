using UnderTest.Attributes;
using UnderTest.Strategy.Selenium.AcceptanceTests.Site;
using UnderTest.Strategy.Selenium.AcceptanceTests.StepParams.Enums;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"Components/WaitUntil.feature")]
  public class WaitUntilFeatureHandler : SiteFeatureHandlerBase
  {
    [Given("our test page")]
    public void ElementExists() =>
      Site.WaitUntil.Visit();

    [When("we wait for the element to be <what-to-wait-for>")]
    public void WeCaptureThePageElement(WaitUntilEnum whatToWaitFor) =>
      Site.WaitUntil.WaitForElement(whatToWaitFor);

    [Then("the element is <what-to-wait-for>")]
    public void ThisExists(WaitUntilEnum whatToWaitFor) =>
      Site.WaitUntil.VerifyElementExistence(whatToWaitFor);
  }
}
