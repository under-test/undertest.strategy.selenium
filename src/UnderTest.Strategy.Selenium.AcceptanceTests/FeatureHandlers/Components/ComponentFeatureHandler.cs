using FluentAssertions;
using OpenQA.Selenium;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"Components/Component.feature")]
  public class ComponentFeatureHandler: LocalAssetsFileSeleniumFeatureHandlerBase
  {
    private ExampleComponent component;

    [Given("our test page")]
    public void OurTestPage()
    {
      TestFilePath = TestFilePath / "component" / "BasicComponent.html";
      LoadLocalTestFile();
    }

    [When("we capture the Component")]
    public void WhenWeClickTheButton()
    {
      component = this.FindComponent<ExampleComponent>();
    }

    [Then("the Component should have the properties of the PageElement")]
    public void ThenTheElementShouldHaveBeenClicked()
    {
      component.GetAttribute("lang").Should().Be("en");
    }

    [Then("the child properties should be available")]
    public void TheChildPropertiesShouldBeAvailable()
    {
      component.Title.Text.Should().Be("Title");
      component.Value.Text.Should().Be("Value");
    }

    [Selector(".the-component")]
    public class ExampleComponent : ComponentBase
    {
      public ExampleComponent(By selector, ICurrentWebDriverAware page)
        : base(selector, page)
      { }

      public PageElement Title => this.FindPageElement(".title");
      public PageElement Value => this.FindPageElement(".value");
    }
  }
}
