using System;
using FluentAssertions;
using UnderTest.Attributes;
using UnderTest.Strategy.Selenium.AcceptanceTests.StepParams;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"Elements/Checkbox/BasicCheckbox.feature")]
  public class BasicCheckboxFeatureHandler : LocalAssetsFileSeleniumFeatureHandlerBase
  {
    private Selenium.Checkbox element;

    [Given("our test page")]
    public void ElementExists()
    {
      TestFilePath = TestFilePath / "checkbox" / "BasicInteraction.html";
      LoadLocalTestFile();
    }

    [When("we capture the (checked|unchecked) Checkbox")]
    public void WeCaptureThePageElement(string checkedStatus)
    {
      element = this
        .FindPageElement(CheckedStatusToSelector(checkedStatus))
        .ToCheckbox();
    }

    [When("we mark the checkbox as (checked|unchecked)")]
    public void WhenWeMarkCheckStatusAs([CheckboxCheckedStatusToBoolStepParam] bool checkedStatus)
    {
      element.Checked = checkedStatus;
    }

    [Then("the element should be (checked|unchecked)")]
    public void ThenTheElementShouldBeChecked([CheckboxCheckedStatusToBoolStepParam] bool checkedStatus)
    {
      element.Checked.Should().Be(checkedStatus);
    }

    private string CheckedStatusToSelector(string checkedStatus)
    {
      return checkedStatus switch
      {
        "checked" => "#checkbox-checked",
        "unchecked" => "#checkbox-not-checked",
        _ => throw new Exception($"Unknown selector lookup {checkedStatus}")
      };
    }
  }
}
