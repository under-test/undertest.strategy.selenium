using System;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"Elements/Textbox/BasicTextbox.feature")]
  public class BasicTextboxFeatureHandler : LocalAssetsFileSeleniumFeatureHandlerBase
  {
    private Textbox SimpleTextbox => this.FindTextbox("#simple-textbox");

    [Given(" our test page")]
    public void GivenOutTestPage()
    {
      TestFilePath = TestFilePath / "textbox" / "BasicInteraction.html";
      LoadLocalTestFile();
    }

    [When("we enter \"shoes\" into the textbox")]
    public void WhenWeEnterTheValueShoes()
    {
      SimpleTextbox.EnterValue("shoes");
    }

    [Then("the value should be \"shoes\"")]
    public void ThenTheValueShouldBeShoes()
    {
      SimpleTextbox.WaitUntilValueIs("shoes");
    }
  }
}
