using FluentAssertions;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"Elements/Button/BasicButton.feature")]
  public class BasicButtonFeatureHandler : LocalAssetsFileSeleniumFeatureHandlerBase
  {
    [Given("our test page")]
    public void OurTestPage()
    {
      TestFilePath = TestFilePath / "button" / "BasicButton.html";
      LoadLocalTestFile();
    }

    [When("we click the button")]
    public void WhenWeClickTheButton()
    {
      this.FindPageElement("#simple-button").Click();
    }

    [Then("the element should have been clicked")]
    public void ThenTheElementShouldHaveBeenClicked()
    {
      this.FindPageElement("#result").WaitUntilVisible().TextContent.Should().Be("clicked");
    }
  }
}
