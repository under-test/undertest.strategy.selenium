using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using FluentAssertions;
using JetBrains.Annotations;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using UnderTest.Attributes;
using UnderTest.Strategy.Selenium.AcceptanceTests.StepParams;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"Elements/Dropdown/BasicDropdown.feature")]
  public class BasicDropdownFeatureHandler : LocalAssetsFileSeleniumFeatureHandlerBase
  {
    private Dropdown element;

    [Given("our test page")]
    public void ElementExists()
    {
      TestFilePath = TestFilePath / "dropdown" / "BasicInteraction.html";
      LoadLocalTestFile();
    }

    [When("we capture the (selected|not selected) Dropdown")]
    public void WeCaptureThePageElement(string selectedStatus)
    {
      element = this
        .FindPageElement(SelectedStatusToSelector(selectedStatus))
        .ToDropdown();
    }

    [When("we mark the dropdown as selected")]
    public void WhenWeMarkCheckStatusAs()
    {
      element.SetSelectedByIndex(1);
      CurrentDriver.SwitchTo().Alert().Accept();
    }

    [Then("the element should be (selected|not selected)")]
    public void ThenTheElementShouldBeChecked([DropdownSelectedStatusToBoolStepParamAttribute] bool selectedStatus)
    {
      Log.Information($"selected text: {element.SelectedOption?.GetAttribute("innerHtml")}");
      if (selectedStatus)
      {
        element.SelectedOption.GetAttribute("value").Should().Be("selected");
      }
      else
      {
        var selected = element.SelectedOption;
        if (selected != null && element?.GetAttribute("value") != "Nothing selected")
        {
          throw new ArgumentException("Value should be empty");
        }
      }
    }

    private string SelectedStatusToSelector(string selectedStatus)
    {
      return selectedStatus switch
      {
        "selected" => "#selected",
        "not selected" => "#nothing-selected",
        _ => throw new Exception($"Unknown selector lookup {selectedStatus}")
      };
    }
  }
}
