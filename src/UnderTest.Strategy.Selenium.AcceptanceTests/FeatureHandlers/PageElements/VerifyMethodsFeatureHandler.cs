using System;
using FluentAssertions;
using OpenQA.Selenium;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"PageElement/VerifyMethods.feature")]
  public class VerifyMethodsFeatureHandler : LocalAssetsFileSeleniumFeatureHandlerBase
  {
    [Given("our browser is on a page where our test element <exists> onscreen")]
    [Given("our browser is on a page where our test element exists onscreen")]
    [Given("our browser is on a page where our test element does not exist onscreen")]
    public void ElementExists()
    {
      TestFilePath = TestFilePath / "verification" / "visibility.html";
      LoadLocalTestFile();
    }

    [Then("the element should be visible")]
    public void ThisIsVisible() =>
      this.FindPageElement("#visible")
        .ShouldBeVisible();

    [Then("the element should be invisible")]
    public void ThisIsInvisible() =>
      this.FindPageElement("#invisible")
        .ShouldBeInvisible();

    [Then("the element <exists>")]
    public void ThenTheElementExists(string exists)
    {
      switch (exists)
      {
        case "exists":
          this.FindPageElement("#visible")
            .ShouldExist();
          break;
        case "does not exist":
          Action act = () => this.FindPageElement("#does-not-exist").ShouldExist();
          act.Should().Throw<NoSuchElementException>();

          break;
        default:
          throw new ArgumentException($"unknown exists parameter {exists}");
      }
    }
  }
}
