using FluentAssertions;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"PageElement/ScrollToElement.feature")]
  public class ScrollToElementFeatureHandler : LocalAssetsFileSeleniumFeatureHandlerBase
  {
    private PageElement element;

    [Given("our test page")]
    public void ElementExists()
    {
      TestFilePath = TestFilePath / "basic" / "ScrollToElement.html";
      LoadLocalTestFile();
    }

    [When("we scroll to the PageElement")]
    public void WeCaptureThePageElement()
    {
      Log.Information("    looking up and scrolling to element...");
      element = this.FindPageElement("#example")
        .ScrollTo();
    }

    [Then("the element should be visible")]
    public void ThisExists()
    {
      element.Displayed.Should().BeTrue();

      Log.Information("  Element has been scrolled to");
    }
  }
}
