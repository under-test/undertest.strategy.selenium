using FluentAssertions;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"PageElement/FindByText.feature")]
  public class FindByTextFeatureHandler : LocalAssetsFileSeleniumFeatureHandlerBase
  {
    private PageElement element;

    [Given("our test page")]
    public void WeAreOnOurTestPage()
    {
      TestFilePath = TestFilePath / "basic" / "FindByText.html";
      LoadLocalTestFile();
    }

    [When("we find the element with the content 'shoes'")]
    public void WeFindTheElementWithTheContentShoes()
    {
      element = this.FindPageElementByText("Shoes");
    }

    [When("we find the child element with the content 'in a child'")]
    public void WhenWeFindTheChildElementWithTheContentInAChild()
    {
      element = this.FindPageElement("#container")
                    .FindPageElementByText("in a child");
    }

    [When("we find the element by the partial content 'something'")]
    public void WhenWeFindTheElementByThePartialContentSomething()
    {
      element = this.FindPageElementByPartialText("something");
    }

    [Then("we should find the element")]
    public void ThenWeShouldFindTheElement()
    {
      element.Text.Should().Be("Shoes");
    }

    [Then("we should find the child element")]
    public void ThenWeShouldFindTheChildElement()
    {
      element.Text.Should().Be("in a child");
    }

    [Then("we should find the partial element")]
    public void ThenWeShouldFindThePartialElement()
    {
      element.Text.Should().Be("something else");
      element.Id.Should().Be("partial");
    }
  }
}
