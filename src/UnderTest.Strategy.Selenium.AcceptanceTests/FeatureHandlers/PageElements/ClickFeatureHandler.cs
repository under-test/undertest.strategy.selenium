﻿using System;
using FluentAssertions;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"PageElement/Click.feature")]
  public class ClickFeatureHandler: LocalAssetsFileSeleniumFeatureHandlerBase
  {
    private PageElement element;

    [Given("our test page")]
    public void ElementExists()
    {
      TestFilePath = TestFilePath / "basic" / "Click.html";
      LoadLocalTestFile();
    }

    [When("I click call Click() on <context>")]
    public void WhenICallClick(string elementContext)
    {
      element = this.FindPageElement(GetSelectorFromContext(elementContext));
      element.Click();
    }

    [Then("the element should have been clicked")]
    public void TheElementShouldHaveBeenClicked()
    {
      element.GetAttribute("clicked-status").Should().Be("true");
    }

    private string GetSelectorFromContext(string elementContext)
    {
      return elementContext switch
      {
        "basic" => "#clickable-right-away",
        "disabled-for-three-seconds" => "#clickable-after-three-seconds",
        _ => throw new Exception($"Unknown selector context {elementContext}")
      };
    }
  }
}
