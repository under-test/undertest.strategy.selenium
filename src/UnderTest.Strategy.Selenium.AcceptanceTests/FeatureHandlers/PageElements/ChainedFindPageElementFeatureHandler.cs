using FluentAssertions;
using OpenQA.Selenium;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"PageElement/ChainedFindPageElement.feature")]
  public class NestedFindPageElementFeatureHandler : LocalAssetsFileSeleniumFeatureHandlerBase
  {
    private PageElement element;

    [Given("our test page")]
    public void WeAreOnOurTestPage()
    {
      TestFilePath = TestFilePath / "basic" / "ChainedFindPageElement.html";
      LoadLocalTestFile();
    }

    [When("we find the child element with the content 'in a child'")]
    public void WhenWeFindTheChildElementWithTheContentInAChild()
    {
      element = this
        .FindPageElement("#container")
        .FindPageElement(".shoes");
    }

    [When("we locate the second row")]
    public void WhenILocateTheSecondRow()
    {
      element = new Selenium.PageElement(By.Id("table-id"), this)
        .FindPageElement(By.CssSelector("tbody tr:nth-of-type(2) td:nth-of-type(2)"))
        .FindPageElement(By.TagName("a"));
    }

    [When("we locate the second row after waiting")]
    public void WhenILocateTheSecondRowAfterWaiting()
    {
      element = new Selenium.PageElement(By.Id("table-id"), this)
        .FindPageElement(By.CssSelector("tbody tr:nth-of-type(2) td:nth-of-type(2)"))
        .FindPageElement(By.TagName("a"))
        .WaitUntilClickable();
    }

    [Then("we should find the child element")]
    public void ThenWeShouldFindTheChildElement()
    {
      element.Text.Should().Be("in a child");
    }

    [Then("we should find the second row")]
    public void ThenWeShouldFindTheSecondRow()
    {
      element.Text.Should().Be("Google Link");
    }
  }
}
