using FluentAssertions;
using OpenQA.Selenium;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"PageElement/ShadowDom.feature")]
  public class ShadowDomFeatureHandler: LocalAssetsFileSeleniumFeatureHandlerBase
  {
    private PageElement element;
    private ShadowRoot ShadowDom;

    [Given("our test page")]
    public void ElementExists()
    {
      TestFilePath = TestFilePath / "basic" / "ShadowDom.html";
      LoadLocalTestFile();
    }

    [When("we capture the PageElement")]
    public void WeCaptureThePageElement()
    {
      Log.Information("    looking up a shadow dom element...");
      element = this.FindPageElement("#example");
    }

    [Then("the shadow dom element is available")]
    public void ThisExists()
    {
      ShadowDom = element.ShadowDom;

      ShadowDom
        .Should()
        .NotBeNull();

      Log.Information("  ShadowDom Element has located");
    }

    [Then("the shadow dom's children are accessible")]
    public void Children()
    {
      var pTag= ShadowDom.FindElement(By.CssSelector("p")).Should().NotBeNull();

      Log.Information("  Found the shadow DOM p tag");
    }
  }
}
