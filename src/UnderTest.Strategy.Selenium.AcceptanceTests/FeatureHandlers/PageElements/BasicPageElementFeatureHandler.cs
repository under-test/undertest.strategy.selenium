using FluentAssertions;
using OpenQA.Selenium;
using UnderTest.Attributes;
using UnderTest.Strategy.Selenium.AcceptanceTests.StepParams;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"PageElement/BasicPageElement.feature")]
  public class BasicPageElementFeatureHandler : LocalAssetsFileSeleniumFeatureHandlerBase
  {
    private PageElement element;

    [Given("our test page")]
    public void ElementExists()
    {
      TestFilePath = TestFilePath / "basic" / "BasicPageElement.html";
      LoadLocalTestFile();
    }

    [When("we capture the PageElement")]
    public void WeCaptureThePageElement()
    {
      Log.Information("    looking up element...");
      element = this.FindPageElement("#example");
    }

    [When("we capture the PageElement's parent")]
    public void WhenWeCaptureParentElement()
    {
      element = this.FindPageElement("#example")
                    .Parent;
    }

    [When("we capture out test PageElement's background-color")]
    public void WhenBackgroundColor()
    {
      element = this.FindPageElement("#with-blue-background");
    }

    [When("we capture the PageElement by Text")]
    public void WhenWeCaptureByText()
    {
      element = this.FindPageElementByText("shoes");
    }

    [When("we capture out test TextNode element")]
    public void WhenWeCaptureOutTextNodeElement()
    {
      element = this.FindPageElement("#textNodeDesiredText");
    }

    [Then("the PageElement should have the properties of the IWebElement")]
    public void ThisExists()
    {
      var webElement = CurrentDriver.FindElement(By.CssSelector("#example"));

      element.Text.Should().Be(webElement.Text);
      element.TagName.Should().Be(webElement.TagName);
      element.Enabled.Should().Be(webElement.Enabled);
      element.Location.Should().Be(webElement.Location);
      element.Selected.Should().Be(webElement.Selected);
      element.Size.Should().Be(webElement.Size);

      Log.Information("  All properties match up as expected");
    }

    [Then("the PageElement should be the parent element")]
    public void ThenThePageElementShouldBeTheParentElement()
    {
      element.Id.Should().Be("parent");
    }

    [Then("it should be blue")]
    public void ThenItShouldBeBlue()
    {
      element.GetCssValue("background-color").Should().Be("rgba(0, 0, 255, 1)");
    }

    [Then("the title should be \"also shoes\"")]
    public void ThenTitleAlsoShoes()
    {
      element.GetAttribute("title").Should().Be("also shoes");
    }

    [Then("the NodeText should be \"also shoes\"")]
    public void ThenNodeTextShouldBe()
    {
      element.NodeText.Should().Be("Get this");
    }

    [When("we check if an element that <does-exist>")]
    public void WhenWeCheckIfAnElementThat([ExistsToBoolStepParam] bool exists)
    {
      var selector = exists ? "i-exist" : "i-dont-exist";
      element = new PageElement(By.Id(selector), this);
    }

    [Then("when we check if it exists, the answer should be <exist-result>")]
    public void WhenWeCheckIfItExists(bool exists)
    {
      element.Exists.Should().Be(exists);
    }
  }
}
