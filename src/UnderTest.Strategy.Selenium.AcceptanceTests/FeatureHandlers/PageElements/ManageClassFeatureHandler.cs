using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"PageElement/ManageClass.feature")]
  public class ManageClassFeatureHandler : LocalAssetsFileSeleniumFeatureHandlerBase
  {
    private PageElement element;

    [Given("our test page")]
    public void ElementExists()
    {
      TestFilePath = TestFilePath / "basic" / "Class.html";
      LoadLocalTestFile();
    }

    [When("we capture the PageElement")]
    public void WeCaptureThePageElement()
    {
      element = this.FindPageElement("#example");
    }

    [When("we add a new class")]
    public void WhenWeAddAnewClass()
    {
      element.AddClass("added");
    }

    [When("we remove a new class")]
    public void WhenWeRemoveANewClass()
    {
      element.RemoveClass("test2");
    }

    [Then("the PageElement should have the expected classes")]
    public void ThenShouldHaveExpectedClasses()
    {
      element
        .ShouldHaveClass("test1")
        .ShouldHaveClass("test2");
    }

    [Then("the class we added")]
    public void ThenTheClassWeAdded()
    {
      element.ShouldHaveClass("added");
    }

    [Then("the PageElement should only have the class we didn't remove")]
    public void ThenThePageElementShouldOnlyHaveTheClassWeDidntRemove()
    {
      element.ShouldHaveClass("test1");
    }
  }
}
