using System;
using FluentAssertions;
using OpenQA.Selenium;
using UnderTest.Attributes;
using UnderTest.Strategy.Selenium.Exceptions;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"ICurrentWebDriverAware/WaitUntilFailureCases.feature")]
  public class WaitUntilFailureCasesFeatureHandler : LocalAssetsFileSeleniumFeatureHandlerBase
  {
    private Action elementLookup;
    private string whatToWaitFor;

    public override void BeforeFeature(BeforeAfterFeatureContext context)
    {
      base.BeforeFeature(context);

      elementLookup = null;
      whatToWaitFor = null;
    }

    [Given("our test page")]
    public void GivenWeAreOnOurTestPage()
    {
      TestFilePath = TestFilePath / "wait" / "WaitUntil.html";
      LoadLocalTestFile();
    }

    [When("we wait for the element to <what-to-wait-for>")]
    public void WhenWeWaitForTheElementToExist(string whatToWaitFor)
    {
      this.whatToWaitFor = whatToWaitFor;
      var timeout = TimeSpan.FromMilliseconds(500);

      switch (whatToWaitFor)
      {
        case "visible":
          elementLookup = () => this.WaitUntilVisible("#just-here-and-invisible", timeout);
          break;
        case "clickable":
          elementLookup = (() => this.WaitUntilClickable("#just-here-and-invisible", timeout));
          break;
        case "invisible":
          elementLookup = (() => this.WaitUntilInvisible("#just-here-and-visible", timeout));
          break;
        case "exists":
          elementLookup = (() => this.WaitUntilExists("#never-exists", timeout));
          break;
        case "title-equals":
          elementLookup = (() => this.WaitUntilTitleIs("something it will never be", timeout));
          break;
        case "title-contains":
          elementLookup = (() => this.WaitUntilTitleContains("nothing", timeout));
          break;
        case "title-match":
          elementLookup = (() => this.WaitUntilTitleMatch(@"(.*) nothing", timeout));
          break;
        case "url-contains":
          elementLookup = (() => this.WaitUntilUrlContains(@"Nope.html", timeout));
          break;
        case "wait-for-all":
          elementLookup = (() => this.WaitUntilCountExists(".just-here-and-visible", 5, timeout));
          break;
        case "stale":
          elementLookup = (() => this.WaitUntilStale(this.FindPageElement("#just-here-and-visible").Element,
            By.CssSelector("#just-here-and-visible"), "#just-here-and-visible", timeout));
          break;
        case "active-element":
          elementLookup = (() => this.WaitUntilActiveElement("#just-here-and-visible", timeout));
          break;
        default:
          throw new Exception($"Unknown <whatToWaitFor> {whatToWaitFor}");
      }
    }

    [When("we wait for the element with no context")]
    public void WeWaitForTheElementWithNoContext()
    {
      elementLookup = () =>
      {
        var initialWindowCount = CurrentDriver.WindowHandles.Count;
        new Selenium.PageElement(By.Id("example-link"), this).WaitUntilClickable().Click();
        // + 2 what causes this to fail.
        this.WaitUntil(driver => driver.WindowHandles.Count > initialWindowCount + 2, TimeSpan.FromSeconds(1),
          // this is what we are testing
          lookupContext: null);
      };
    }

    [Then("an exception should be thrown with a clear message")]
    public void ThenAnExceptionShouldBeThrown()
    {
      var message = BuildMessageFromWhatToWaitFor();

      elementLookup
        .Should().Throw<WaitTimeoutExceededException>()
        .And
          .Message.Should().Be(message);
    }

    [Then("an exception, without context, should be thrown")]
    public void AnExceptionWithoutContextShouldBeThrown()
    {
      elementLookup
        .Should().Throw<WaitTimeoutExceededException>()
        .And
        .Message.Should().Be("Expected condition failed to occur within the timeout of 1 second(s)");
    }

    private string BuildMessageFromWhatToWaitFor()
    {
      string whatToLookFor;
      switch (whatToWaitFor)
      {
        case "visible":
          whatToLookFor = "visible element By.CssSelector: #just-here-and-invisible";
          break;
        case "clickable":
          whatToLookFor = "clickable element By.CssSelector: #just-here-and-invisible";
          break;
        case "invisible":
          whatToLookFor = "invisible element By.CssSelector: #just-here-and-visible";
          break;
        case "exists":
          whatToLookFor = "element By.CssSelector: #never-exists";
          break;
        case "title-equals":
          whatToLookFor = "page title equal to `something it will never be`";
          break;
        case "title-contains":
          whatToLookFor = "page title containing `nothing`";
          break;
        case "title-match":
          whatToLookFor = @"page title containing `(.*) nothing`";
          break;
        case "url-contains":
          whatToLookFor = @"url containing `Nope.html`";
          break;
        case "wait-for-all":
          whatToLookFor = "there to be 5 By.CssSelector: .just-here-and-visible";
          break;
        case "stale":
          whatToLookFor = "the element By.CssSelector: #just-here-and-visible to go stale";
          break;
        case "active-element":
          whatToLookFor = "that the active element is By.CssSelector: #just-here-and-visible";
          break;
        default:
          throw new Exception($"Unknown <whatToWaitFor> {whatToWaitFor}");
      }

      return $"Failed to locate {whatToLookFor} within the timeout of 0.5 second(s)";
    }
  }
}
