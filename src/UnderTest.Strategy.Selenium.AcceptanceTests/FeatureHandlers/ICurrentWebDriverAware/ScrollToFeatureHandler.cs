using System;
using FluentAssertions;
using UnderTest.Attributes;

// ReSharper disable once CheckNamespace
namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"ICurrentWebDriverAware/ScrollTo.feature")]
  public class ScrollToFeatureHandler : LocalAssetsFileSeleniumFeatureHandlerBase
  {
    [Given("our browser test page")]
    public void Given()
    {
      TestFilePath = TestFilePath / "basic" / "ScrollTo.html";
      LoadLocalTestFile();
    }

    [When("we scroll to <location>")]
    public void When(string location)
    {
      switch (location)
      {
        case "the bottom":
          this.ScrollToBottom();
          break;
        case "the top":
          this.ScrollToTop();
          break;
        case "a specific-location":
          this.ScrollTo(0, 500);
          break;
        default:
          throw new ArgumentException("Unknown scroll to location");
      }
    }

    [Then("the scroll location should be <location>")]
    public void Then(string location)
    {
      switch (location)
      {
        case "the bottom":
          this.GetScrollLocation().Should().BeGreaterThan(5000);
          break;
        case "the top":
          this.GetScrollLocation().Should().Be(0);
          break;
        case "a specific-location":
          this.GetScrollLocation().Should().Be(500);
          break;
        default:
          throw new ArgumentException("Unknown scroll to location");
      }
    }
  }
}
