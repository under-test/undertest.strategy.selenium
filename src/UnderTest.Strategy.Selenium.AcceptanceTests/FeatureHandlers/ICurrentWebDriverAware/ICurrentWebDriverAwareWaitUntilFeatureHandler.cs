using System;
using System.Linq;
using FluentAssertions;
using OpenQA.Selenium;
using UnderTest.Attributes;

// ReSharper disable once CheckNamespace
namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"ICurrentWebDriverAware/WaitUntil.feature")]
  public class ICurrentWebDriverAwareWaitUntilFeatureHandler : LocalAssetsFileSeleniumFeatureHandlerBase
  {
    private PageElement element;

    [Given("our test page")]
    public void ElementExists()
    {
      TestFilePath = TestFilePath / "wait" / "WaitUntil.html";
      LoadLocalTestFile();
    }

    [When("we wait for the element to be <what-to-wait-for>")]
    public void WeCaptureThePageElement(string whatToWaitFor)
    {
      switch (whatToWaitFor)
      {
        case "visible":
          element = this.WaitUntilVisible("#hidden-for-three-seconds");
          break;
        case "clickable":
          element = this.WaitUntilClickable("#disabled-for-three-seconds");
          break;
        case "invisible":
          element = this.WaitUntilInvisible("#will-hide-after-three-seconds");
          break;
        case "exists":
          element = this.WaitUntilExists("#exists-after-three-seconds");
          break;
        case "title-equals":
          this.WaitUntilTitleIs("updated");
          break;
        case "title-contains":
          this.WaitUntilTitleContains("something");
          break;
        case "title-match":
          this.WaitUntilTitleMatch(@"(.*) something");
          break;
        case "url-contains":
          this.WaitUntilUrlContains(@"WaitUntil.html");
          break;
        case "wait-for-none":
          this.WaitUntilCountExists(".does-not-exist", 0);
          break;
        case "wait-for-all":
          this.WaitUntilCountExists(".wait-for-all", 5);
          break;
        case "stale":
          this.WaitUntilStale(this.FindPageElement("#will-be-stale").Element, By.CssSelector("#will-be-stale"));
          break;
        case "active-element":
          element = this.WaitUntilActiveElement("#focus-after-three-seconds");
          break;
        case "child-element":
          element = this.WaitUntilChildExistsInSelector("#wait-for-child span", ele => ele.Text == "3").Last();
          break;
        case "text-exists":
          element = this.WaitUntilTextExists("I was appended!");
          break;
        default:
          throw new Exception($"Unknown <whatToWaitFor> {whatToWaitFor}");
      }
    }

    [Then("the element is <what-to-wait-for>")]
    public void ThisExists(string whatToWaitFor)
    {
      switch (whatToWaitFor)
      {
        case "visible":
          element.Displayed.Should().BeTrue();
          break;
        case "clickable":
          (element.Displayed && element.Enabled).Should().BeTrue();
          break;
        case "invisible":
          element.Displayed.Should().BeFalse();
          break;
        case "exists":
          element.Should().NotBeNull();
          break;
        case "title-equals":
          CurrentDriver.Title.Should().Be("updated");
          break;
        case "title-contains":
          CurrentDriver.Title.Should().Contain("something");
          break;
        case "title-match":
          CurrentDriver.Title.Should().Contain("something");
          break;
        case "url-contains":
          CurrentDriver.Url.Should().Contain("WaitUntil.html");
          break;
        case "wait-for-none":
          this.FindPageElements(".wait-for-none").Count.Should().Be(0);
          break;
        case "wait-for-all":
          this.FindPageElements(".wait-for-all").Count.Should().Be(5);
          break;
        case "stale":
          this.FindPageElements("#will-be-stale").Count.Should().Be(0);
          break;
        case "active-element":
          element.Equals(CurrentDriver.SwitchTo().ActiveElement()).Should().BeTrue();
          break;
        case "child-element":
          element.Text.Should().Be("3");
          break;
        case "text-exists":
          Console.WriteLine(element.Text);
          element.Text.Should().Be("I was appended!");
          break;
        default:
          throw new Exception($"Unknown <whatToWaitFor> {whatToWaitFor}");
      }

      Log.Information($"  Waited for element to be {whatToWaitFor}");
    }
  }
}
