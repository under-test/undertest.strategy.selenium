﻿using System;
using FluentAssertions;
using UnderTest.Attributes;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"ICurrentWebDriverAware/CheckFor.feature")]
  public class CheckForFeatureHandlerBase : LocalAssetsFileSeleniumFeatureHandlerBase
  {
    private PageElement element;

    private PageElement Container => this.FindPageElement("#container");

    [Given("our test page")]
    public void ElementExists()
    {
      TestFilePath = TestFilePath / "wait" / "CheckFor.html";
      LoadLocalTestFile();
    }

    [When("we check for the element to be <what-to-check-for>")]
    public void WeCaptureThePageElement(string whatToCheckFor)
    {
      switch (whatToCheckFor)
      {
        case "simple-check":
          element = this.CheckForExistence<PageElement>("#container");
          break;
        case "non-existent":
          element = this.CheckForExistence<PageElement>("#i-dont-exist");
          break;
        case "nested-that-exists":
          element = Container.CheckForExistence<PageElement>("#i-exist-nested");
          break;
        case "nested-doesnt-exist":
          element = Container.CheckForExistence<PageElement>("#i-exist-not-in-the-container");
          break;
        default:
          throw new Exception($"Unknown <whatToCheckFor> {whatToCheckFor}");
      }
    }

    [Then("the element is <what-to-check-for>")]
    public void ThisExists(string whatToCheckFor)
    {
      Log.Information($"  Check for element to be {whatToCheckFor}");

      switch (whatToCheckFor)
      {
        case "simple-check":
        case "nested-that-exists":
          element.Displayed.Should().BeTrue();
          break;
        case "non-existent":
        case "nested-doesnt-exist":
          element.Should().BeNull();
          break;
        default:
          throw new Exception($"Unknown <whatToCheckFor> {whatToCheckFor}");
      }
    }
  }
}
