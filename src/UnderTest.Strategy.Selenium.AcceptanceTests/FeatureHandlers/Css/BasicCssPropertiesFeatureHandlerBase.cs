using UnderTest.Attributes;
using UnderTest.Strategy.Selenium.AcceptanceTests.Site;

namespace UnderTest.Strategy.Selenium.AcceptanceTests.FeatureHandlers
{
  [HandlesFeature(@"Css/BasicCssProperties.feature")]
  public class BasicCssPropertiesFeatureHandlerBase: SiteFeatureHandlerBase
  {
    [Given("our test page")]
    public void OurTestPage() =>
      Site.CssBasicProperties.Visit();

    [When("we capture the element")]
    public void WeCaptureThePageElement() =>
      Site.CssBasicProperties.VerifyBackgroundExists();

    [Then("our example image should be returned")]
    public void ThenTheElementShouldBeChecked() =>
      Site.CssBasicProperties.VerifyBackgroundIsOurExampleImage();
  }
}
