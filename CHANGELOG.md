# Changelog

## 0.2.0 - 2021-01-04
### added 
- adds `GetExecutingFolder` extension to `ICurrentWebDriverAware` #17
- adds access to Selector from PageElement #20 
- adds access to ShadowDom directly through a property on PageElement #19
- adds ExecuteScript to PageElement #19
- PageElement now implements IWrapsElement #19 
- adds constructor to wrap an element #19
- adds SetAttribute to PageElement #22
- adds ScrollTo to PageElement #13
- adds TextContent and InnerHtml to PageElement #23
- adds FindPageElement to `ICurrentWebDriverAware` #27
- adds Perform wrapper to PageElement #21
- adds support for WaitXX helpers #18
- adds methods ShouldBeVisible and ShoWaitUntilClickableuldBeInvisible to PageElement #32
- adds `DropFileOntoElement` to PageElement #33
- adds method to WaitUntilInvisible #41
- adds `ScrollTo`, `ScrollToBottom` and `ScrollToTop` methods to `ICurrentWebDriverAware` #42
- adds a Click with offsets to PageElement #39
- adds a `Parent` property to `PageElement` #34
- adds a `ShouldExist()` to `PageElement` #36 
- adds `GetClasses`, `AddClass`, `RemoveClass`, `ShouldHaveClass`,`ShouldNotContainClass` to PageElement #35 
- adds `ToCheckbox()` to `PageElement` with `Checked`, `Value` and `Readonly` #45
- adds `WaitUntilCountExists` and `WaitUntilStale` to `ICurrentWebDriverAware` #43
- adds `SeleniumTestStrategy` #3
- adds `ToDropdown()` to `PageElement` with Dropdown type for interacting with dropdowns #47
- adds `IPageObject` interface #52
- adds `FindPageElementByText` and `FindPageElementsByText` #53
- adds `WaitUntilActiveElement` to `ICurrentWebDriverAware` #49
- adds `Css` property to `PageElement` #48
- adds basic screenshot capture #7
- adds a `WaitUntilAvailable` method #70
- adds a `EnableDemoMode` and `SetDefaultWaitPollInterval` to `SeleniumTestStrategy` #74
- adds a `ComponentBase` #77
- adds a `Button` #76
- adds a `Textbox` element #81
- adds a `SiteBase` #79
- adds FindComponent and ToComponent methods #103
- adds a LocalAssetsFileSeleniumFeatureHandlerBase #104
- adds a method for `WaitUntilChildExistsInSelector` #109
- adds a `SetDefaultTimeouts`

## 0.1.0 - 2019-09-22
- initial version
