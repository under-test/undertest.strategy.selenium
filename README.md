# UnderTest.Strategy.Selenium

![UnderTest](https://paper-attachments.dropbox.com/s_83978FCF913FDC2F4078354E63EC6B9F5369CBAC7A4A8452BDBD67B665B61908_1560127143078_logo1.png)
    
[![Active](http://img.shields.io/badge/Status-Active-green.svg)](https://gitlab.com/under-test/undertest.strategy.selenium) 
[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](https://gitlab.com/under-test/undertest.strategy.selenium/blob/stable/LICENSE)
[![NuGet version (UnderTest)](https://img.shields.io/nuget/v/UnderTest.Strategy.Selenium.svg?style=flat-square)](https://www.nuget.org/packages/UnderTest.Strategy.Selenium/)
[![Build status](https://ci.appveyor.com/api/projects/status/lg1rbg46rk8fdnhu/branch/stable?svg=true)](https://ci.appveyor.com/api/projects/status/lg1rbg46rk8fdnhu/branch/stable?svg=true)
[![NuGet version with pre-releases (UnderTest)](https://img.shields.io/nuget/vpre/UnderTest.Strategy.Selenium.svg?style=flat-square)](https://www.nuget.org/packages/UnderTest/)

## What is UnderTest.Strategy.Selenium?

Set of tools and extensions for UnderTest when using Selenium.

## Getting Started

### Project Setup

1. [Install UnderTest.Templates](https://gitlab.com/under-test/undertest.templates)
1. run `dotnet new UnderTest --name "MyProject"` - [see Usage for other options](https://gitlab.com/under-test/undertest.templates#usage)
1. Add the package by executing `dotnet add package UnderTest.Strategy.Selenium`
1. Open the project and have fun.

## Fundamentals

This strategy enhances the use of Selenium within UnderTest.

### ICurrentWebDriverAware

This interface marks a type as aware of the current `IWebDriver`. Implementing this allows the library to access the Driver with a shared approach.  

### PageElement

When working with Selenium's `IWebElement` in a raw form, you end up managing two things:

1. the selector to attempt to locate element(s)
1. the actual cached element from the page

`PageElement` wraps those two concerns into one facade that simplifies page object code and increases readability.

To create a `PageElement`, 

```csharp
var thing = new PageElement("#header", this);
```

after that - treat `thing` as you would an `IWebElement` wrapper of PageElement.

> In the call above `this` is an instance of `ICurrentWebDriverAware`.

## License

UnderTest is a free open source project, released under the permissible MIT License. This means it can be used in personal and commercial projects for free.  MIT is the same license used by such popular projects as Angular and Dotnet.
